# 18.12.12 // frode burdal klevstul // www.thisworld.is

"""
the feed catcher will download a feed, given a service_id and bundle_id as
input parameters. bundle_id is optional.

$ python feed_catcher.py [service_id] [bundle_id]
"""

def main():
    """
    main module
    """
    import sys
    import pickle
    from mlib.database import Database
    from mlib.error import output as error_output
    from mlib.load import data as load_data
    from mlib.prepare import content_data as prepare_content_data

    # service_id is a mandatory input parameter
    if len(sys.argv) < 2:
        error_output('missing argument: service_id', True)
    service_id = sys.argv[1]

    # bundle_id is an optional input parameter
    bundle_id = None
    if len(sys.argv) > 2:
        bundle_id = sys.argv[2]

    # create a database instance
    database = Database(
        {
            'service_id': service_id,
            'bundle_id': bundle_id,
            'cache': 'false',
            'debug': 'false'
        }
    )

    # load the content
    feed = load_data(database.data)

    # prepare the content
    feed = prepare_content_data(database.data, feed)

    # store the content
    with open(database.data.filename_feed, 'wb') as handle:
        pickle.dump(feed, handle)
        handle.close()


if __name__ == "__main__":
    main()
