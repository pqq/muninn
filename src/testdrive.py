# 18.10.15 // frode burdal klevstul // www.thisworld.is

"""
test script that execute muninn's test functions
"""

from mlib.test_imports import do_import as test_imports_do_imports

from mlib.conversion import test_json_to_dictionary as conversion_test_json_to_dictionary
from mlib.conversion import (
    test_pub_date_to_display_date as conversion_test_pub_date_to_display_date
)
from mlib.database import test_database as database_test_database
from mlib.datastructure import test_datastructure as datastructure_test_datastructure
from mlib.debug import test_add_to_debug_dict as debug_test_add_to_debug_dict
from mlib.debug import test_output as debug_test_output
from mlib.environment import test_get as environment_test_get
from mlib.error import test_output as error_test_output
from mlib.error import test_output_as_json as error_test_output_as_json
from mlib.error import test_return_as_entries as error_test_return_as_entries
from mlib.extract import test_columns_from_row as extract_test_columns_from_row
from mlib.extract import test_entry_title_elements as extract_test_entry_title_elements
from mlib.extract import test_feed_special_code as extract_test_feed_special_code
from mlib.extract import test_feed_title_elements as extract_test_feed_title_elements
from mlib.filter import test_feed as filter_test_feed
from mlib.gitlab import test_get_data as gitlab_test_get_data
from mlib.load import test_load as load_test_load
from mlib.log import test_request as log_test_request
from mlib.markdown import test_to_html as markdown_test_to_html
from mlib.output import test_dict_as_json as output_test_dict_as_json
from mlib.output import test_feed as output_test_feed
from mlib.output import test_web_resource as output_test_web_resource
from mlib.parameters import test_get as parameters_test_get
from mlib.prepare import test_content_data as prepare_test_content_data
from mlib.search import test_feed as search_test_feed
from mlib.sort import test_feed as sort_test_feed
from mlib.version import test_get as version_test_get
from mlib.www import test_asciify as www_test_asciify
from mlib.www import test_get_parameters as www_test_get_parameters
from mlib.www import test_getweb_resource as www_test_getweb_resource
from mlib.www import test_html_to_text as www_test_html_to_text
from mlib.www import test_load_pickle as www_test_load_pickle
from mlib.www import test_load_rss_feed as www_test_load_rss_feed
from mlib.www import test_print_header as www_test_print_header
from mlib.www import test_url_decode as www_test_url_decode
from mlib.www import test_url_encode as www_test_url_encode


try:
    test_imports_do_imports()

    conversion_test_json_to_dictionary()
    conversion_test_pub_date_to_display_date()
    database_test_database()
    datastructure_test_datastructure()
    debug_test_output()
    debug_test_add_to_debug_dict()
    environment_test_get()
    error_test_output()
    error_test_output_as_json()
    error_test_return_as_entries()
    extract_test_feed_special_code()
    extract_test_feed_title_elements()
    extract_test_entry_title_elements()
    extract_test_columns_from_row()
    filter_test_feed()
    gitlab_test_get_data()          # makes http requests
    load_test_load()                # makes http requests
    log_test_request()
    markdown_test_to_html()
    output_test_feed()
    output_test_dict_as_json()
    output_test_web_resource()      # makes http requests
    parameters_test_get()
    prepare_test_content_data()     # makes http requests
    search_test_feed()
    sort_test_feed()
    version_test_get()
    www_test_print_header()
    www_test_get_parameters()
    www_test_getweb_resource()
    www_test_load_pickle()
    www_test_load_rss_feed()        # makes http requests
    www_test_asciify()
    www_test_url_encode()
    www_test_url_decode()
    www_test_html_to_text()
    print('all tests were successfully performed. muninn is ready to fly!')
except ValueError as error:
    import sys
    print(error.args[0] + ': ' + error.args[1])
    sys.exit(1)
