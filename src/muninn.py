# 18.10.15 // frode burdal klevstul // www.thisworld.is

#@ #! /var/www/hom/bin/python @ENV_PROD @INTERPRETER

"""
muninn (mind) - where it all starts, at least if we include consciousness
"""

def main():
    """
    master of actions. coordinating it all.
    """
    import time
    from mlib.database import Database
    from mlib.filter import feed as filter_feed
    from mlib.load import data as load_data
    from mlib.output import dict_as_json as output_dict_as_json
    from mlib.output import feed as output_feed
    from mlib.parameters import get as parameters_get
    from mlib.prepare import content_data as prepare_content_data
    from mlib.search import feed as search_feed
    from mlib.sort import feed as sort_feed
    from mlib.log import request as log_request

    # get the timestamp for when muninn starts
    start_time = time.time()

    # get input parameters
    parameters = parameters_get()

    # create a new database instance (load database)
    database = Database(parameters)

    # load the data
    feed = load_data(database.data)

    # store the start time in the feed, to be used for calculating the total
    # timing of the muninn request
    feed['time_total'] = start_time

    # huginn data
    if parameters['bundle_id'] == 'huginn':

        # log request
        if database.data.log:
            log_request(database.data)

        # output the feed for the world to see
        output_dict_as_json(database.data, feed)

    # content data
    else:

        # prepare the content data, making sure it's on the right format,
        # if it is not selected from cache as cached data is already prepared
        if 'false' in feed['cache_use_status']:
            feed = prepare_content_data(database.data, feed)

        # sort the data
        if parameters['sort'] and not 'error' in feed['entries']:
            feed = sort_feed(feed)

        # search the data
        if parameters['search'] and not 'error' in feed['entries']:
            feed = search_feed(feed)

        # filter the data
        if not 'error' in feed['entries']:

            # filter feed - needs to be done as the last operation before
            # outputing - reason: to return the correct number of entries
            # in case a size limit is set
            feed = filter_feed(database.data, feed)

        # output the feed for the world to see
        output_feed(database.data, feed)


if __name__ == '__main__':
    main()
