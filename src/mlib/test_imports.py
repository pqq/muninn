# 19.03.29 // frode burdal klevstul // www.thisworld.is

"""
used for testing imports of non built in python modules
"""

# pylint: disable=unused-import

def do_import():
    """
    test imports
    """
    try:
        import feedparser
    except ImportError:
        import traceback
        raise ValueError(
            'test_imports.do_import()',
            'feedparser import error \n------------------\n' +
            traceback.format_exc() + '------------------')

    try:
        import markdown
    except ImportError:
        import traceback
        raise ValueError(
            'test_imports.do_import()',
            'markdown import error \n------------------\n' +
            traceback.format_exc() + '------------------')
