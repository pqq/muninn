# 18.11.05 // frode burdal klevstul // www.thisworld.is

"""
data extraction functions
"""

def feed_special_code(fsc, code_to_get):
    """
    get and return a feed special code
    """
    return_value = None
    # lower and add the always present '='
    code_to_get = code_to_get.lower() + '='

    # if the code is found...
    if code_to_get.lower() in fsc.lower():
        length_of_code = len(code_to_get)
        # lower
        return_value = fsc.lower()
        # remove everything in front of the code to get
        return_value = return_value[(return_value.find(code_to_get)+length_of_code):]
        # find the first space after the value we are after, in case more special params given
        space_index = return_value.find(' ')
        # in case no space (index being -1), then set index to length of string, for avoid
        # loss of last character
        if space_index < 0:
            space_index = len(return_value)
        # remove all after the value
        return_value = return_value[:space_index]

    return return_value


def test_feed_special_code():
    """
    test
    """
    fsc = 'foo=bar bar=foo muninn=huginn'
    muninn = None
    try:
        muninn = feed_special_code(fsc, 'muninn')
    except:
        import traceback
        raise ValueError(
            'extract.test_feed_special_code()',
            'unable to retreive muninn\n------------------\n' +
            traceback.format_exc() + '------------------')
    if not muninn == 'huginn':
        raise ValueError('extract.test_feed_special_code()', 'muninn is not huginn')


def feed_title_elements(title):
    """
    extract data from a feed title
    """
    from mlib.www import asciify as www_asciify

    title_data = None

    if title:

        title_splitted = title.split('|')
        feed_profile = title_splitted[0].rstrip()
        feed_profile_id = www_asciify(feed_profile.lower())

        try:
            feed_country_code = title_splitted[1].lstrip().rstrip()
        except IndexError:
            feed_country_code = 'NOTFOUND'

        try:
            feed_main_category = title_splitted[2].lstrip().rstrip()
            feed_main_category_id = www_asciify(feed_main_category)
        except IndexError:
            feed_main_category = 'NOTFOUND'
            feed_main_category_id = feed_main_category

        try:
            feed_sub_category = title_splitted[3].lstrip().rstrip()
            feed_sub_category_id = www_asciify(feed_sub_category)
        except IndexError:
            feed_sub_category = 'NOTFOUND'
            feed_sub_category_id = feed_sub_category

        try:
            feed_source = title_splitted[4].lstrip().rstrip()
            feed_source_id = www_asciify(feed_source)
        except IndexError:
            feed_source = 'NOTFOUND'
            feed_source_id = feed_source

        try:
            feed_s_c = title_splitted[5].lstrip().rstrip()
        except IndexError:
            feed_s_c = 'NOTFOUND'

        try:
            feed_id = title_splitted[6].lstrip().rstrip()
        except IndexError:
            feed_id = 'NOTFOUND'


        title_data = {
            'feed_profile':           feed_profile,
            'feed_profile_id':        feed_profile_id,
            'feed_country_code':      feed_country_code,
            'feed_main_category':     feed_main_category,
            'feed_main_category_id':  feed_main_category_id,
            'feed_sub_category':      feed_sub_category,
            'feed_sub_category_id':   feed_sub_category_id,
            'feed_source':            feed_source,
            'feed_source_id':         feed_source_id,
            'feed_special_code':      feed_s_c,
            'feed_id':                feed_id
        }

    return title_data


def test_feed_title_elements():
    """
    test
    """
    title = ('frode burdal klevstul | eng | maincat | subcat | gitlab | '
             'ff=1 gts=0 ws=1 pli=0 edd=1 | 1811182104')
    try:
        title_data = feed_title_elements(title)
    except:
        import traceback
        raise ValueError(
            'extract.test_feed_title_elements()',
            'initiation issues\n------------------\n' +
            traceback.format_exc() + '------------------')
    if not title_data:
        raise ValueError('extract.test_feed_title_elements()', 'no title_data')
    if ('feed_profile_id' and 'feed_country_code' and 'feed_id') not in title_data:
        raise ValueError('extract.test_feed_title_elements()', 'missing elements')
    if title_data['feed_profile_id'] != 'frodeburdalklevstul':
        raise ValueError('extract.test_feed_title_elements()', 'missing data')


def entry_title_elements(path):
    """
    extract data given a path

    example input path:
        181227#my_dir_is_my_title/181227-this_is_my_title-tag1_tag2.md
    """

    element_separator = '-'
    space_separator = '_'
    tags_separator = '_'

    # we shall try to extract interesting data from the given path
    extracted_title_data = None
    filename = None

    # remove the trailing slash
    if path.endswith('/'):
        path = path[:-1]

    # if there are more slashes, slash away... or, throw away everything before the slash
    if '/' in path:
        path = path.split('/')[-1]

    # store the filename, inc filetype if it exsists
    filename = path

    # check if the remaning string is on the format 000000-sometext...
    if path and len(path) > 5 and path[:6].isdigit() and path[6] == element_separator:
        path_splitted = path.split(element_separator)

        date = ('20' + path_splitted[0][:2] + '-' + path_splitted[0][2:4] + '-' +
                path_splitted[0][4:6] + 'T00:00:00.000+01:00')
        date_display = ('20' + path_splitted[0][:2] + '.' + path_splitted[0][2:4] + '.' +
                        path_splitted[0][4:6] + ' 00:00 UTC')
        date_nerd = path_splitted[0] + '000000'

        try:
            entry_title = path_splitted[1]
            entry_title = entry_title.replace(space_separator, ' ')
        except IndexError:
            entry_title = 'NOTFOUND'

        try:
            entry_tags = path_splitted[2].split(tags_separator)
        except IndexError:
            entry_tags = ''

        extracted_title_data = {
            'date': date,
            'date_display': date_display,
            'date_nerd': date_nerd,
            'entry_title': entry_title,
            'entry_tags': entry_tags
        }

    # not a specially formatted title, so we can't extract any extra info
    else:

        extracted_title_data = {
            'date': '',
            'date_display': '',
            'date_nerd':'',
            'entry_title':(filename if path else ''),
            'entry_tags': ''
        }

    return extracted_title_data


def test_entry_title_elements():
    """
    test
    """
    title = '180876-frode_burdal_klevstul-klok_lærd_vis'
    try:
        title_data = entry_title_elements(title)
    except:
        import traceback
        raise ValueError(
            'extract.test_postTitleElements()',
            'initiation issues\n------------------\n' +
            traceback.format_exc() + '------------------')
    if not title_data:
        raise ValueError('extract.test_postTitleElements()', 'lost in cyberspace')
    if not title_data['date_nerd'] == '180876000000':
        raise ValueError('extract.test_postTitleElements()', 'a birthday issue')
    if not 'klok' in title_data['entry_tags']:
        raise ValueError('extract.test_postTitleElements()', 'do not act stupid')


def columns_from_row(row, keep):
    """
    filter out all rows except those named in 'keep'
    """
    filtered_row = {}

    for key, value in row.items():
        if key in keep:
            filtered_row[key] = value

    return filtered_row


def test_columns_from_row():
    """
    test
    """
    row = {'one': 1, 'two': 2, 'three': 3}
    keep = None
    try:
        keep = columns_from_row(row, ['one', 'three'])
    except:
        import traceback
        raise ValueError(
            'extract.test_postTitleElements()',
            'initiation issues\n------------------\n' +
            traceback.format_exc() + '------------------')
    if 'two' in keep:
        raise ValueError(
            'extract.test_postTitleElements()',
            'we have more than we say we want #hipsterissue')
    if ('one' and 'three') not in keep:
        raise ValueError('extract.test_postTitleElements()', 'we are missing things')
