# 18.11.04 // frode burdal klevstul // www.thisworld.is

"""
functions used for error handling
"""

def output(message, do_exit=False):
    """
    print error message
    """
    import sys

    print('\nERROR: ' + message + '\n')
    if do_exit:
        sys.exit()


def test_output():
    """
    test
    """
    import io
    from contextlib import redirect_stdout
    try:
        with io.StringIO() as buf, redirect_stdout(buf):
            print('fbk')
            output_ = buf.getvalue()
    except:
        import traceback
        raise ValueError(
            'error.test_output()',
            'initiation issue\n------------------\n' +
            traceback.format_exc() + '------------------')
    if not 'fbk' in output_:
        raise ValueError('error.test_output()', 'f to the b to the k is missing')


def output_as_json(message, do_exit=False, print_header=True):
    """
    print error message in json format
    """
    import sys
    import json

    error_object = {}
    error_object['error'] = message
    if print_header:
        print('Content-Type: application/json; charset="UTF-8"')
        print('Access-Control-Allow-Origin: *')
        print()
    print(json.dumps(error_object, sort_keys=True, indent=4, separators=(',', ': ')))
    if do_exit:
        sys.exit()


def test_output_as_json():
    """
    test
    """
    import io
    from contextlib import redirect_stdout
    try:
        with io.StringIO() as buf, redirect_stdout(buf):
            output_as_json('fbk')
            the_output = buf.getvalue()
    except:
        import traceback
        raise ValueError(
            'error.test_output_as_json()',
            'initiation issue\n------------------\n' +
            traceback.format_exc() + '------------------')
    if 'fbk' not in the_output:
        raise ValueError('error.test_output_as_json()', 'f to the b to the k is not in da house')


def return_as_entries(message):
    """
    return the error message as entries (sort of)
    """
    entries = {'error': message}
    return entries


def test_return_as_entries():
    """
    test
    """
    msg = return_as_entries('hello_world')
    if 'error' not in msg:
        raise ValueError('error.test_return_as_entries()', 'guru meditation')
    if not msg['error'] == 'hello_world':
        raise ValueError('error.test_return_as_entries()', 'no greeting')
