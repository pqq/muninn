# 18.11.12 // frode burdal klevstul // www.thisworld.is

"""
function for determining the environment that muninn is running on. note that this must
be updated so it's valid for the arcitechture it's running on as directory names are
used for setting the name of the executing environment.
"""

def get():
    """
    return correct environment

    list of valid environments:
    https://gitlab.com/klevstul/muninn/wikis/environments-and-services
    """
    import os
    from mlib.error import output_as_json as error_output_as_json

    # check what directory we are running from
    directory = os.path.dirname(os.path.realpath(__file__))

    environment = 'unknownenvironment:' + directory

    # the beta environment has got the name "beta" in the directory path
    if '/beta' in directory.lower():
        environment = 'beta'
    # the prod environment has got the name "prod" in the directory path
    elif '/prod' in directory.lower():
        environment = 'prod'
    # docker test image
    elif '/builds' in directory.lower():
        environment = 'docker'
    # the development environment has got "Dropbox" in its directory path
    elif '/dropbox' in directory.lower():
        environment = 'dev'
    else:
        error_output_as_json('environment: unable to get environment from path [' +
                             directory.lower() + ']', True)

    return environment


def test_get():
    """
    test
    """
    env_id = get()
    if not env_id in ['dev', 'beta', 'prod', 'docker']:
        raise ValueError('environment.test_get()', 'environment error [' + env_id +']')
