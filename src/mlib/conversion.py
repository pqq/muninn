# 18.11.12 // frode burdal klevstul // www.thisworld.is

"""
misc convertion functions
"""

def json_to_dictionary(json_data):
    """
    deseralise json into a python object (dictionary)
    ref: https://docs.python.org/2/library/json.html
    """
    import json
    from json import JSONDecodeError
    from mlib.error import output as error_output

    dic = {}

    try:
        dic = json.loads(json_data)
    except JSONDecodeError as jde:
        error_output('invalid json data at ' +
                     jde.pos + ' - ' +
                     jde.lineno + ' - ' +
                     jde.colno
                     , True
                    )
    except TypeError as tye:
        error_output(str(tye) + '\n\n' + str(json_data), True)

    return dic


def test_json_to_dictionary():
    """
    test
    """
    import json
    json_data = json.dumps({"why": "because", "hour": -8})
    dict_data = json_to_dictionary(json_data)
    if dict_data['hour'] == -8:
        _test_result = True
    else:
        raise ValueError('test_json_to_dictionary()', 'hurdal, we have a problem...')


def pub_date_to_display_date(pub_date, date_format):
    """
    format the rss pub date (eg. 'Tue, 16 Aug 2016 01:28:47 +0000') to:
    - the sortable 'nerd date' format (eg. '20160816012847')
    - the readable 'display date' format (eg. '2016.08.16 01:28 CET')
    """
    import time
    import feedparser

    # parse it into a standard Python 9-tuple
    # - https://pythonhosted.org/feedparser/date-parsing.html

    # pylint: disable=protected-access
    parsed_date = feedparser._parse_date(pub_date)

    # convert python date to what i call "nerd date", the most logical date
    # format of them all (https://docs.python.org/2/library/time.html)
    if date_format == 'nerd':
        parsed_date_string = time.strftime('%Y%m%d%H%M%S', parsed_date)
    # display date, perfect for presenting to the peeps
    elif date_format == 'display':
        parsed_date_string = time.strftime('%Y.%m.%d %H:%M UTC', parsed_date)
    else:
        parsed_date_string = 'unknown_date_format'

    return parsed_date_string


def test_pub_date_to_display_date():
    """
    test
    """
    try:
        pub_date = 'Wed, 18 Aug 1976 02:20:00 +0000'
        nerd_date = pub_date_to_display_date(pub_date, 'nerd')
    except:
        import traceback
        raise ValueError(
            'test_pub_date_to_display_date()',
            'string to nerd date conversion problem\n------------------\n' +
            traceback.format_exc() + '------------------'
            )
    if not len(nerd_date) == 14:
        raise ValueError('test_pub_date_to_display_date()', 'wrong length of nerd date')
