# 18.11.28 // frode burdal klevstul // www.thisworld.is

"""
handle muninn version details
"""

def get():
    """
    return version name and number
    """
    version_details = {
        'number':     '2',
        'name':       'bil'
    }

    return version_details


def test_get():
    """
    test
    """
    try:
        ver = get()
    except:
        import traceback
        raise ValueError(
            'version.test_get()',
            'initiation issue\n------------------\n' +
            traceback.format_exc() + '------------------')
    try:
        _ver_no = ver['number']
    except:
        import traceback
        raise ValueError(
            'version.test_get()',
            'strong mind, yes, but is a good heart appreciated enough?\n------------------\n' +
            traceback.format_exc() + '------------------')
