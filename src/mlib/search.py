# 18.11.09 // frode burdal klevstul // www.thisworld.is

"""
search functions
"""

def feed(feed_data):
    """
    search feed content, based on search parameter given

    prefix codes explained:
    ti - title include
    te - title exclude
    bi - body include
    be - body exclude
    gi - tag include
    ge - tag exclude

    example:    'ti.welcome-ti.frode-be.exclude_me'
    meaning:    include all entries with title that contains the strings 'welcome'
                and 'frode' and where body does not contain 'exclude me'
                (underscore is replaces with space)
    """
    from mlib.debug import add_to_debug_dict as debug_add_to_debug_dict

    entries = feed_data['entries']
    search_parameter = feed_data['parameters']['search']
    search_elements = search_parameter.split('-')
    search_debug_object = {}

    # loop through all entries (note that i have to use range instead for normal 'for x in y' to
    # avoid problems when using remove())
    for index in range(len(entries) - 1, -1, -1):
        row = entries[index]

        # store the title and the body of the entry into variables
        title = row['entry_title'].lower()
        body = row['entry_content_text'].lower()
        tags = row['entry_tags']

        # loop through all elements given in the search parameter
        for element in search_elements:

            # get the string part of the element, the part after the dot, and replace underscore
            # with space
            string = element[element.find('.')+1:].replace('_', ' ')

            # title include
            if 'ti.' in element and string not in title:
                feed_data['entries'].remove(row)
                search_debug_object = debug_add_to_debug_dict(
                    {'entry_title': row['entry_title'],
                     'entry_published_date_display': row['entry_published_date_display'],
                     'feed_profile_id': row['feed_profile_id'],
                     'search_value_causing_removal': element},
                    search_debug_object, 'title_include', row['entry_id'])
                break

            # body include
            elif 'bi.' in element and string not in body:
                feed_data['entries'].remove(row)
                search_debug_object = debug_add_to_debug_dict(
                    {'entry_title': row['entry_title'],
                     'entry_published_date_display': row['entry_published_date_display'],
                     'feed_profile_id': row['feed_profile_id'],
                     'search_value_causing_removal': element},
                    search_debug_object, 'body_include', row['entry_id'])
                break

            # tag include
            elif 'gi.' in element and string not in tags:
                feed_data['entries'].remove(row)
                search_debug_object = debug_add_to_debug_dict(
                    {'entry_title': row['entry_title'],
                     'entry_published_date_display': row['entry_published_date_display'],
                     'feed_profile_id': row['feed_profile_id'],
                     'search_value_causing_removal': element},
                    search_debug_object, 'tag_include', row['entry_id'])
                break

            # title exclude
            elif 'te.' in element and string in title:
                feed_data['entries'].remove(row)
                search_debug_object = debug_add_to_debug_dict(
                    {'entry_title': row['entry_title'],
                     'entry_published_date_display': row['entry_published_date_display'],
                     'feed_profile_id': row['feed_profile_id'],
                     'search_value_causing_removal': element},
                    search_debug_object, 'title_exclude', row['entry_id'])
                break

            # body exclude
            elif 'be.' in element and string in body:
                feed_data['entries'].remove(row)
                search_debug_object = debug_add_to_debug_dict(
                    {'entry_title': row['entry_title'],
                     'entry_published_date_display': row['entry_published_date_display'],
                     'feed_profile_id': row['feed_profile_id'],
                     'search_value_causing_removal': element},
                    search_debug_object, 'body_exclude', row['entry_id'])
                break

            # tag exclude
            elif 'ge.' in element and string in tags:
                feed_data['entries'].remove(row)
                search_debug_object = debug_add_to_debug_dict(
                    {'entry_title': row['entry_title'],
                     'entry_published_date_display': row['entry_published_date_display'],
                     'feed_profile_id': row['feed_profile_id'],
                     'search_value_causing_removal': element},
                    search_debug_object, 'tag_exclude', row['entry_id'])
                break

        feed_data['debug_object']['excluded_from_searching'] = search_debug_object

    feed_data['entries'] = entries

    return feed_data


def test_feed():
    """
    test
    """
    from mlib.test_data import get_test_entries
    parameters = {}
    parameters['debug'] = 'false'
    parameters['search'] = 'ti.two'
    search_feed_data = {
        'parameters': parameters,
        'debug_object': {},
        'entries': get_test_entries()
    }
    try:
        out_feed = feed(search_feed_data)
    except:
        import traceback
        raise ValueError(
            'search.test_feed()',
            'initiation issue\n------------------\n' +
            traceback.format_exc() + '------------------')
    # ti - title include
    if out_feed['entries'][0]['entry_id'] != 2:
        raise ValueError('search.test_feed()', 'ti.failed')
    # te - title exclude
    search_feed_data['parameters']['search'] = 'te.one'
    out_feed = feed(search_feed_data)
    if out_feed['entries'][0]['entry_id'] != 2:
        raise ValueError('search.test_feed()', 'te.failed')
    # bi - body include
    search_feed_data['parameters']['search'] = 'bi.two'
    out_feed = feed(search_feed_data)
    if out_feed['entries'][0]['entry_id'] != 2:
        raise ValueError('search.test_feed()', 'bi.failed')
    # be - body exclude
    search_feed_data['parameters']['search'] = 'be.one'
    out_feed = feed(search_feed_data)
    if out_feed['entries'][0]['entry_id'] != 2:
        raise ValueError('search.test_feed()', 'be.failed')
    # gi - tag include
    search_feed_data['parameters']['search'] = 'gi.twotone'
    out_feed = feed(search_feed_data)
    if out_feed['entries'][0]['entry_id'] != 2:
        raise ValueError('search.test_feed()', 'gi.failed')
    # ge - tag exclude
    search_feed_data['parameters']['search'] = 'ge.onetone'
    out_feed = feed(search_feed_data)
    if out_feed['entries'][0]['entry_id'] != 2:
        raise ValueError('search.test_feed()', 'ge.failed')
