# 18.11.09 // frode burdal klevstul // www.thisworld.is

"""
sorting of data
"""

def feed(feed_data):
    """
    sort feed, based on sort string given as a parameter
    """
    import sys
    from mlib.error import return_as_entries as error_return_as_entries

    entries = feed_data['entries']
    sort = feed_data['parameters']['sort']
    dot_index = 0

    if sort not in (None, ''):
        # find the position of the dot ('.'), in the 'sort' parameter
        # and if there is no '.' in the 'sort' parameter set the index at the end (length)
        dot_index = sort.find('.')
        if dot_index == -1:
            dot_index = len(sort)

        try:
            # sorted(iterable[, cmp[, key[, reverse]]])
            # ref: https://docs.python.org/2/library/functions.html#sorted
            # ref: https://docs.python.org/2/howto/sorting.html#sortinghowto

            # sort example values:
            #   - 'feed_profile_id'
            #   - 'feed_profile_id.reverse'
            # 'sort[:dot_index]'
            #   - the element to sort on (eg 'feed_profile_id')
            # 'key=lambda k: k[sort[:dot_index]]'
            #   - eg 'key=lambda k: k['feed_profile_id']',
            #     which means sorting the result set on the profile id
            # 'reverse=('reverse' in sort)'
            #   - becomes true or false, depending on the string 'reverse' being in the 'sort'
            #     parameter, or not
            entries = sorted(
                entries, key=lambda k: k[sort[:dot_index]], reverse=('reverse' in sort))
        except KeyError:
            entries = error_return_as_entries(
                'most likely trying to sort on an element that do not exist ' +
                format(sys.exc_info()))

    feed_data['entries'] = entries
    feed_data['debug_object']['sorting'] = sort

    return feed_data


def test_feed():
    """
    test
    """
    from mlib.test_data import get_test_entries
    sort_feed_data = {
        'parameters': {'debug': 'false', 'sort': 'entry_title.reverse'},
        'debug_object': {},
        'entries': get_test_entries()
    }
    try:
        out_feed = feed(sort_feed_data)
    except:
        import traceback
        raise ValueError(
            'sort.test_feed()',
            'initiation issue\n------------------\n' +
            traceback.format_exc() + '------------------')
    if out_feed['entries'][0]['entry_id'] != 2:
        raise ValueError('sort.test_feed()', 'logic; is it preventing us from seeing the truth?')
