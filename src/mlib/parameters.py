# 18.11.04 // frode burdal klevstul // www.thisworld.is

"""
handling of all parameters muninn needs for operating
"""

def get():
    """
    get all parameters post/get requests
    """
    from mlib.error import output_as_json as error_output_as_json
    from mlib.www import get_parameters as www_get_parameters
    from mlib.www import url_encode as www_url_encode
    import os

    # determine if muninn runs from command line interface (cli) or through a web browser
    running_from_cli = True
    if 'REQUEST_METHOD' in os.environ:
        running_from_cli = False

    return_parameters = {}

    # every parameter to be received as a http post request parameter has to be declared in the
    # default_parameters dict
    default_parameters = {
        'service_id':               ('atestservice' if running_from_cli else None),
        # DEBUG_VALUES:             gitlab_atestservice - fno - huginn
        'bundle_id':                None,
        # DEBUG_VALUES:             list_entries_json - list_entries_rss - list_profiles_json
        'method':                   ('list_entries_json' if running_from_cli else None),
        'callback':                 None,
        'cache':                    None,
        'entry_id':                 None,
        # DEBUG_VALUES:             'coding-writing'
        'entry_tags':               None,
        'feed_country_code':        None,
        'feed_main_category_id':    None,
        'feed_sub_category_id':     None,
        'feed_profile_id':          None,
        'feed_source_id':           None,
        'feed_id':                  None,
        # DEBUG_VALUES:             '*.*' - 'test/links.md' - 'index%2Findex.md'
        # (note: path is to be used with bundle_id gitlab_atestservice)
        'path':                     None,
        # DEBUG_VALUES:             b0eefa890e29200d1ce6778312a2431002f04b19
        # (note: file_id is to be used with bundle_id gitlab_atestservice)
        'file_id':                  None,
        'max_concurrent_entries':   None,
        # DEBUG_VALUES:             'feed_country_code.eng'
        'exclude':                  None,
        # DEBUG_VALUES:             'entry_link-entry_title'
        'avoid_dupes':              None,
        'cl_feed_main_category_id': None,
        'cl_feed_profile_id':       None,
        # DEBUG_VALUES:             1
        'list_size':                None,
        # DEBUG_VALUES:             'entry_id.reverse'
        'sort':                     None,
        # DEBUG_VALUES:             'ti.aktiv'  'ge.coding'
        'search':                   None,
        'debug':                    ('true' if running_from_cli else None)
    }

    http_parameters = www_get_parameters()

    # build return_parameters dictionary
    for i in default_parameters:
        # load parameter from post/get request
        if http_parameters.getvalue(i):
            return_parameters[i] = www_url_encode(http_parameters.getvalue(i))
        # else use the default value defined above, when running from cli
        else:
            return_parameters[i] = www_url_encode(default_parameters[i])

    # check that we have the mandatory parameters
    if return_parameters['service_id'] is None:
        error_output_as_json('parameters: \'service_id\' missing from request', True)
    if return_parameters['bundle_id'] != 'huginn' and return_parameters['file_id'] is None:
        if return_parameters['method'] is None:
            error_output_as_json('parameters: \'method\' missing from request', True)

    return return_parameters


def test_get():
    """
    test
    """
    try:
        parameters = get()
    except:
        import traceback
        raise ValueError(
            'parameters.test_get()',
            'initiation issue\n------------------\n' +
            traceback.format_exc() + '------------------')
    if len(parameters) != 24:
        raise ValueError('parameters.test_get()', 'wrong number of parameters')
