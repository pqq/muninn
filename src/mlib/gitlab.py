# 18.10.28 // frode burdal klevstul // www.thisworld.is

"""
muninn's gitlab integration
"""

# comment on code quality and disabling pylint checks:
#     this is "legacy code" from before using pylint for checking code quality.
#     the code fails on
#         - too many local variables
#         - too many statements
#     instead of doing a complete rewrite of i've simply added pylint exceptions to
#     disable the relevant checks.
#
# pylint: disable=too-many-locals, too-many-statements

def get_data(db_data, project, file_id=None, path=None):
    """
    get and return gitlab data
    """

    import json
    from mlib.conversion import json_to_dictionary as conversion_json_to_dictionary
    from mlib.datastructure import Entry
    from mlib.debug import add_to_debug_dict as debug_add_to_debug_dict
    from mlib.error import output_as_json as error_output_as_json
    from mlib.extract import entry_title_elements as extract_entry_title_elements
    from mlib.extract import feed_title_elements as extract_feed_title_elements
    from mlib.markdown import to_html as markdown_to_html
    from mlib.output import web_resource as output_web_resource
    from mlib.www import getweb_resource as www_getweb_resource
    from mlib.www import html_to_text as www_html_to_text
    from mlib.www import url_encode as www_url_encode

    gitlab_debug_object = {}

    # get/set gitlab specific data
    try:
        gitlab_user = db_data.core['gitlab']['user']
        gitlab_api_private_token = (
            db_data.core['gitlab']['api_private_token']
            .replace(':personal_access_token', db_data.core['gitlab']['personal_access_token'])
        )
        default_exma_file = db_data.core['gitlab']['default_exma_file']
        alternative_exma_file = db_data.core['gitlab']['alternative_exma_file']
        default_file_type = db_data.core['gitlab']['default_file_type']
        gitlab_api_project = (
            db_data.core['gitlab']['api_root'] +
            db_data.core['gitlab']['api_project'] + '?' + gitlab_api_private_token
        )
        gitlab_api_repository_tree = (
            db_data.core['gitlab']['api_root'] + db_data.core['gitlab']['api_repository_tree'] +
            '&' + gitlab_api_private_token
        )
        gitlab_api_blob_raw = (
            db_data.core['gitlab']['api_root'] + db_data.core['gitlab']['api_blob_raw'] +
            '?' + gitlab_api_private_token)
        gitlab_api_file = (
            db_data.core['gitlab']['api_root'] + db_data.core['gitlab']['api_file'] +
            '&' + gitlab_api_private_token
        )
        gitlab_api_file_raw = (
            db_data.core['gitlab']['api_root'] + db_data.core['gitlab']['api_file_raw'] +
            '&' + gitlab_api_private_token
        )
        gitlab_url_raw = (
            db_data.core['gitlab']['url_raw'].replace(':user', gitlab_user)
            .replace(':project_name', project))
        gitlab_src_link_code = db_data.core['gitlab']['src_link_code']
        gitlab_src_link_editor_url = db_data.core['gitlab']['src_link_editor_url']
        project_url = gitlab_api_project.replace(':id', gitlab_user+'%2F'+project)
    except NameError:
        error_output_as_json('gitlab: gitlab specific data was not found in the database', True)


    def __get_repository_tree(project_id, path=None):
        """
        get a repository tree's root or subdirectory if path is given
        this call lists all content in the given folder

        example urls:
            https://gitlab.com/api/v4/projects/9948451/repository/tree
            https://gitlab.com/api/v4/projects/9948451/repository/tree?path=orec
        """
        import time

        local_gitlab_debug_object = gitlab_debug_object

        repository_tree_url = gitlab_api_repository_tree.replace(':id', project_id)
        if path:
            repository_tree_url = repository_tree_url + '&path=' + path

        repository_tree = conversion_json_to_dictionary(www_getweb_resource(repository_tree_url))
        # adding timestamp to dict debug name to prevent overwriting the previous stored value
        local_gitlab_debug_object = debug_add_to_debug_dict(
            {'repository_tree_url-' + str(time.time()): repository_tree_url},
            local_gitlab_debug_object)

        return repository_tree


    def __parse_repository_recursively(project_id, repository_tree):
        """
        recursively parse through all content in a repository
        """
        entries_on_current_level = []

        # loop through all files on current level
        for repository_file in repository_tree:

            entry_title = repository_file['path']

            # add a slash at the end of the titles that are representing a tree/directory
            if repository_file['type'] == 'tree':
                entry_title = entry_title + '/'

            # parse the feed title and get relevant title elements
            feed_title_elements = extract_feed_title_elements(db_data.feed_title)

            # binary object files
            if repository_file['type'] == 'blob':

                entry_link = (
                    gitlab_api_blob_raw.replace(':id', str(gitlab_project['id']))
                    .replace(':sha', str(repository_file['id']))
                )

            # directory / tree
            elif repository_file['type'] == 'tree':

                repository_tree = __get_repository_tree(project_id, repository_file['path'])
                entries_on_sub_levels = __parse_repository_recursively(id, repository_tree)

                for entry in entries_on_sub_levels:
                    entries_on_current_level.append(entry)

                entry_link = gitlab_api_repository_tree.replace(':id', str(gitlab_project['id']))

            entry = Entry()
            Entry().setattribute(entry, 'entry_content_html', '')
            Entry().setattribute(entry, 'entry_content_text', '')
            Entry().setattribute(entry, 'entry_id', repository_file['id'])
            Entry().setattribute(entry, 'entry_link', entry_link)
            Entry().setattribute(entry, 'entry_number', 0)
            Entry().setattribute(entry, 'entry_published_date', '')
            Entry().setattribute(entry, 'entry_published_date_display', '')
            Entry().setattribute(entry, 'entry_published_date_nerd', '')
            Entry().setattribute(entry, 'entry_tags', '')
            Entry().setattribute(entry, 'entry_title', entry_title)
            Entry().setattribute(
                entry, 'feed_country_code', feed_title_elements['feed_country_code']
            )
            Entry().setattribute(entry, 'feed_id', feed_title_elements['feed_id'])
            Entry().setattribute(
                entry, 'feed_main_category', feed_title_elements['feed_main_category']
            )
            Entry().setattribute(
                entry, 'feed_main_category_id', feed_title_elements['feed_main_category_id']
            )
            Entry().setattribute(entry, 'feed_profile', feed_title_elements['feed_profile'])
            Entry().setattribute(entry, 'feed_profile_id', feed_title_elements['feed_profile_id'])
            Entry().setattribute(entry, 'feed_source', feed_title_elements['feed_source'])
            Entry().setattribute(entry, 'feed_source_id', feed_title_elements['feed_source_id'])
            Entry().setattribute(
                entry, 'feed_special_code', feed_title_elements['feed_special_code']
            )
            Entry().setattribute(
                entry, 'feed_sub_category', feed_title_elements['feed_sub_category']
            )
            Entry().setattribute(
                entry, 'feed_sub_category_id', feed_title_elements['feed_sub_category_id']
            )

            # add the current entry
            entries_on_current_level.append(entry.unload())

        return entries_on_current_level


    def __create_entry(db_data, entry_id, entry_path, content):
        """
        return a feed entry based on input data
        """

        # '!' (exclamation mark - exma) is a "special" character that might exist in the path,
        # if it does we will try to open a file directly in the folder, instead of listing
        # the folder content. if the path ends with '!' we will try to open a default file.
        # if a string is after the '!' we try to open that filename instead. md files only.
        exma_character = '!'
        exma_alternative = '-'
        exma_file_to_open = ''
        if exma_character in entry_path:
            if entry_path.endswith(exma_character):
                exma_file_to_open = www_url_encode('/') + default_exma_file
            elif entry_path.endswith(exma_alternative):
                exma_file_to_open = www_url_encode('/') + alternative_exma_file
            else:
                exma_file_to_open = (
                    www_url_encode('/') + entry_path.split(exma_character)[-1] + default_file_type
                )

        feed_title_elements = extract_feed_title_elements(db_data.feed_title)
        entry_title_elements = extract_entry_title_elements(entry_path.split(exma_character)[0])

        entry_tags = entry_title_elements['entry_tags']
        entry_published_date = entry_title_elements['date']
        entry_published_date_nerd = entry_title_elements['date_nerd']
        entry_published_date_display = entry_title_elements['date_display']
        entry_title = entry_title_elements['entry_title']

        entry_link = (
            'muninn://service_id=' + db_data.parameters['service_id'] +
            '&method=' + db_data.parameters['method'] + '&path=' + www_url_encode(entry_path) +
            exma_file_to_open
        )
        if db_data.parameters['bundle_id']:
            entry_link += '&bundle_id=' + db_data.parameters['bundle_id']

        entry = Entry()
        Entry().setattribute(entry, 'entry_content_html', content)
        Entry().setattribute(entry, 'entry_content_text', www_html_to_text(content))
        Entry().setattribute(entry, 'entry_id', entry_id)
        Entry().setattribute(entry, 'entry_link', entry_link)
        Entry().setattribute(entry, 'entry_number', 0)
        Entry().setattribute(entry, 'entry_published_date', entry_published_date)
        Entry().setattribute(entry, 'entry_published_date_display', entry_published_date_display)
        Entry().setattribute(entry, 'entry_published_date_nerd', entry_published_date_nerd)
        Entry().setattribute(entry, 'entry_tags', entry_tags)
        Entry().setattribute(entry, 'entry_title', entry_title)
        Entry().setattribute(entry, 'feed_country_code', feed_title_elements['feed_country_code'])
        Entry().setattribute(entry, 'feed_id', feed_title_elements['feed_id'])
        Entry().setattribute(entry, 'feed_main_category', feed_title_elements['feed_main_category'])
        Entry().setattribute(
            entry, 'feed_main_category_id', feed_title_elements['feed_main_category_id']
        )
        Entry().setattribute(entry, 'feed_profile', feed_title_elements['feed_profile'])
        Entry().setattribute(entry, 'feed_profile_id', feed_title_elements['feed_profile_id'])
        Entry().setattribute(entry, 'feed_source', feed_title_elements['feed_source'])
        Entry().setattribute(entry, 'feed_source_id', feed_title_elements['feed_source_id'])
        Entry().setattribute(entry, 'feed_special_code', feed_title_elements['feed_special_code'])
        Entry().setattribute(entry, 'feed_sub_category', feed_title_elements['feed_sub_category'])
        Entry().setattribute(
            entry, 'feed_sub_category_id', feed_title_elements['feed_sub_category_id']
        )

        return entry.unload()


    def __update_relative_paths(content, base_url):
        """
        change from relative to full paths
        """
        import re

        # a full url encoding will mess up the path. it is sufficient to encode spaces.
        base_url = base_url.replace(' ', '%20')

        # split content into a list of lines
        content_splitlines = content.splitlines()

        # declare a parameter where we'll store the return content
        content_return = ''

        # loop through, line by line
        for line in content_splitlines:
            # example line:
            #   ![](dir%20w%20(paren)/pic1.jpg) [a](http://a.no) ![](dir%20w%20(paren)/pic2.jpg)

            # we're only interested in lines with markdown image links
            if '![' in line and '](' in line and ')' in line:

                # split on ')' without removing the delimiter
                #   https://stackoverflow.com/questions
                #       /2136556/in-python-how-do-i-split-a-string-and-keep-the-separators
                # result of example line after split:
                #   ['![](dir%20w%20(paren', ')', '/pic1.jpg', ')', ' [a](http://a.no', ')',
                #       ' ![](dir%20w%20(paren', ')', '/pic2.jpg', ')', '']
                elements = re.split(r'(\))', line)

                new_elements = []

                # loop through all elements of the line
                for elem in elements:
                    # we're only interest in the parts that contains image links
                    if '![' in elem:
                        # relative paths do not start with http, so those are the one we're
                        # looking for
                        if not '](http' in elem:
                            elem = elem.replace('](', '](' + base_url)
                        new_elements.append(elem)
                    else:
                        new_elements.append(elem)
                content_return += ''.join(new_elements)
            else:
                content_return += line + '\n'

        return content_return


    # -----------------------------------------------
    # main part of the method is starting here
    # -----------------------------------------------

    # load the gitlab project
    gitlab_project = json.loads(www_getweb_resource(project_url))
    gitlab_debug_object = debug_add_to_debug_dict({'project_url': project_url}, gitlab_debug_object)

    # get the id from the gitlab project
    project_id = str(gitlab_project['id'])

    # get path for current project
    project_path = str(gitlab_project['path_with_namespace'])

    # declare the return data
    feed = {}
    feed['debug_object'] = {}
    entries = []
    feed['entries'] = entries

    # -----
    # blob / raw file - if a file id is given we are returning the raw file content
    # -----
    if file_id:

        blob_raw_url = gitlab_api_blob_raw.replace(':id', project_id).replace(':sha', file_id)

        # output of raw web resource will exit the program (hence no need to log any debugging
        # info, as it will be lost in space)
        output_web_resource(blob_raw_url)

    # -----
    # present the content of a supported file
    # -----
    elif path and (
            path.lower().endswith(default_file_type) or
            path.lower().endswith('.markdown') or
            path.lower().endswith('.txt') or
            path.lower().endswith('.html')
    ):

        # retreive file information
        # example url:
        # https://gitlab.com/api/v4/projects/9948451/repository/files
        # /orec%2F181219-visits_from_north-test_personal-list%2Freadme.md?ref=master&private_token=
        file_url = gitlab_api_file.replace(':id', project_id).replace(':file_path', path)
        file = json.loads(www_getweb_resource(file_url))
        gitlab_debug_object = debug_add_to_debug_dict({'file_url': file_url}, gitlab_debug_object)

        # get the base url, or the full url to the file, in case of the content being referenced
        # with a relative path
        base_url = gitlab_url_raw + file['file_path']
        base_url = base_url.rsplit('/', 1)[0] + '/'

        # determine the url to the raw file
        file_raw_url = gitlab_api_file_raw.replace(':id', project_id).replace(':file_path', path)

        # append source link code (slc) to the html content
        append_slc = ''
        if gitlab_src_link_code:
            gitlab_src_link_editor_url = (
                gitlab_src_link_editor_url.replace('[project_path]', project_path)
                .replace('[ref]', file['ref']).replace('[file_path]', file['file_path'])
            )
            append_slc = (
                gitlab_src_link_code
                .replace('[editor]', gitlab_src_link_editor_url)
                .replace('[raw]', file_raw_url)
            )

        # download the file content
        file_raw = www_getweb_resource(file_raw_url)
        file_raw = __update_relative_paths(file_raw.decode('utf-8'), base_url)
        file_html = markdown_to_html(file_raw) + append_slc
        gitlab_debug_object = debug_add_to_debug_dict(
            {'file_raw_url': file_raw_url}, gitlab_debug_object)

        # create a new entry
        entries.append(__create_entry(db_data, file['blob_id'], file['file_path'], file_html))

    # -----
    # recursively list all content in a repository
    # -----
    elif path == '*.*':

        repository_tree = __get_repository_tree(project_id)
        entries = __parse_repository_recursively(project_id, repository_tree)

        # it would be tricky setting the correct entry number in __parse_repository_recursively(),
        # hence this is done afterwards
        entry_number = 0
        for entry in entries:
            entry_number = entry_number + 1
            entry['entry_number'] = entry_number

    # -----
    # list folders
    # -----
    else:

        repository_tree = __get_repository_tree(project_id, path)
        entry_number = 0

        for repository_file in repository_tree:
            entries.append(__create_entry(
                db_data, repository_file['id'], repository_file['path'], ''))

    feed['entries'] = entries
    feed['debug_object']['gitlab'] = gitlab_debug_object

    return feed


def test_get_data():
    """
    test
    """
    from mlib.parameters import get as parameters_get
    from mlib.database import Database
    parameters = parameters_get()
    database = Database(parameters)
    database.data.feed_title = 'fbk | nor | hkat | ukat | gitlab | ff=1 | 1808760220'
    try:
        _feed = get_data(db_data=database.data, project='atestservice', file_id=None, path=None)
    except:
        import traceback
        raise ValueError(
            'gitlab.test_getData()',
            'unable to load feed\n------------------\n' +
            traceback.format_exc() + '------------------')
