# 18.12.10 // frode burdal klevstul // www.thisworld.is

"""
data structure class
"""

class DS:
    """
    class used for defining data structures
    """


    def forceload(self, dic):
        """
        force load all dict values into object attributes
        """
        for key, value in dic.items():
            setattr(self, key, value)


    def load(self, dic):
        """
        load dict values into object attributes.
        this only sets attributes that are already defined in the class.
        this is done to ensure data correctness.
        """
        for key, value in dic.items():
            if hasattr(self, key):
                setattr(self, key, value)


    @staticmethod
    def setattribute(obj, key, value):
        """
        function that only sets attributes that already do exist, to ensure data correctness.
        """
        if hasattr(obj, key):
            setattr(obj, key, value)


    def unload(self):
        """
        return object attributes as dict
        """
        ret = {}
        for attr, value in self.__dict__.items():
            ret[attr] = value

        return ret


    def validate(self, obj):
        """
        validate attributed in a given object with self
        """
        for attr, _value in obj.__dict__.items():
            if not hasattr(self, attr):
                return False

        return True


class Entry(DS):
    """
    an entry in the feed
    """
    entry_content_html = ''
    entry_content_text = ''
    entry_id = ''
    entry_link = ''
    entry_number = 0
    entry_published_date = ''
    entry_published_date_display = ''
    entry_published_date_nerd = ''
    entry_tags = []
    entry_title = ''
    feed_country_code = ''
    feed_id = ''
    feed_main_category = ''
    feed_main_category_id = ''
    feed_profile = ''
    feed_profile_id = ''
    feed_source = ''
    feed_source_id = ''
    feed_special_code = ''
    feed_sub_category = ''
    feed_sub_category_id = ''


    def __init__(self):
        pass


class Feed(DS):
    """
    the feed, or the complete json return object
    """
    cache_use_status = ''
    debug_object = {}
    feed_title = ''
    hits = 0
    parameters = []
    entries = []
    time_feed_loading = 0
    time_total = 0
    version_details = {}


def test_datastructure():
    """
    test
    """
    try:
        feed = Feed()
        dic = {'cache_use_status': 'true', 'nonexistingattr': 'getlostinspace'}
        feed.load(dic)
        dic = feed.unload()
    except:
        import traceback
        raise ValueError(
            'datastructure.test_datastructure()',
            'initiation issue\n------------------\n' +
            traceback.format_exc() + '------------------')
    if 'nonexistingattr' in dic:
        raise ValueError('datastructure.test_datastructure()', 'found in space')
    if 'cache_use_status' not in dic:
        raise ValueError('datastructure.test_datastructure()', 'lost in space')
