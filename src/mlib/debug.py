# 18.11.04 // frode burdal klevstul // www.thisworld.is

"""
debugging functions
"""

def output(message, simple=False):
    """
    print debug messages
    """
    from mlib.environment import get as environment_get

    if environment_get() == 'prod':
        do_debug = False
    else:
        do_debug = True

    if simple and do_debug:
        print('# ' + message)
    elif do_debug:
        print('\nDEBUG: ' + message + '\n')


def test_output():
    """
    test
    """
    import os
    import sys
    try:
        sys.stdout = open(os.devnull, 'w')
        output('say o nara')
    except:
        import traceback
        raise ValueError(
            'debug.test_output()',
            'initiation issue\n------------------\n' +
            traceback.format_exc() + '------------------')
    finally:
        sys.stdout = sys.__stdout__


def add_to_debug_dict(input_dict, debug_dict=None, sub_key_name=None, sub_sub_key_name=None):
    """
    add data to a debug dictionary
    """

    # in case you were not given a fict we will created one
    if debug_dict is None:
        debug_dict = {}

    # in case the input data are to be added to a sub key in the debug dict
    if sub_key_name and not sub_key_name in debug_dict:
        debug_dict[sub_key_name] = {}

    # in case the input data is to be added to a second sub key
    if sub_key_name and sub_sub_key_name and not sub_sub_key_name in debug_dict[sub_key_name]:
        debug_dict[sub_key_name][sub_sub_key_name] = {}

    # add all on the data in the input dict to the debug dict
    for key, value in input_dict.items():
        if sub_sub_key_name:
            debug_dict[sub_key_name][sub_sub_key_name][key] = value
        elif sub_key_name:
            debug_dict[sub_key_name][key] = value
        else:
            debug_dict[key] = value

    return debug_dict


def test_add_to_debug_dict():
    """
    test
    """
    debug_dict = None
    try:
        try:
            debug_dict = add_to_debug_dict({'foo': 1, 'bar': 'two'}, None, 'level_one', 'level_two')
        except:
            import traceback
            raise ValueError(
                'debug.test_add_to_debug_dict()',
                'unable to create debug dictionary\n------------------\n' +
                traceback.format_exc() + '------------------')
        if not debug_dict:
            raise ValueError('debug.test_add_to_debug_dict()', 'debug_dict empty after creation')
        if 'level_one' not in debug_dict:
            raise ValueError('debug.test_add_to_debug_dict()', 'sub_key error')
        if 'level_two' not in debug_dict['level_one']:
            raise ValueError('debug.test_add_to_debug_dict()', 'sub_sub_key error')
        if 'foo' not in debug_dict['level_one']['level_two']:
            raise ValueError('debug.test_add_to_debug_dict()', 'foo is missing')
        if debug_dict['level_one']['level_two']['bar'] != 'two':
            raise ValueError('debug.test_add_to_debug_dict()', 'bar has wrong value')
    except ValueError as error:
        raise ValueError(error.args[0], error.args[1])
