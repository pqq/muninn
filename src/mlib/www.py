# 18.10.15 // frode burdal klevstul // www.thisworld.is

"""
world wide web related functions
"""

def print_header(content_type='text/html', extra_info=False):
    """
    outputs the header for a web page
    """
    print('Content-Type: '+content_type+'; charset="UTF-8"')
    if extra_info:
        print(extra_info)
    print()


def test_print_header():
    """
    test
    """
    import io
    from contextlib import redirect_stdout
    try:
        with io.StringIO() as buf, redirect_stdout(buf):
            print_header('la83b')
            output = buf.getvalue()
    except:
        import traceback
        raise ValueError(
            'www.test_print_header()',
            'initiation issue\n------------------\n' +
            traceback.format_exc() + '------------------')
    if not 'la83b' in output:
        raise ValueError('www.test_print_header()', 'flexnes calling lost in space')


def get_parameters():
    """
    retrieves and returns all http request parameters
    """
    import cgi
    #import cgitb; cgitb.enable() # for debugging

    parameters = cgi.FieldStorage()

    return parameters


def test_get_parameters():
    """
    test
    """
    try:
        par = get_parameters()
    except:
        import traceback
        raise ValueError(
            'www.test_get_parameters()',
            'initiation issue\n------------------\n' +
            traceback.format_exc() + '------------------')
    try:
        _none_value = par.getvalue('noOne')
    except:
        import traceback
        raise ValueError(
            'www.test_get_parameters()',
            'no one in the entrire universe, but yourself\n------------------\n' +
            traceback.format_exc() + '------------------')


def getweb_resource(url):
    """
    loads and returns a resource on the net
    """
    import urllib.request
    import urllib.error
    from mlib.error import output_as_json as error_output_as_json

    try:
        http_response_object = urllib.request.urlopen(url)
        web_content = http_response_object.read()
        http_response_object.close()
        return web_content
    except urllib.error.HTTPError as error:
        error_output_as_json(
            {'type': 'HTTPError', 'code': str(error.code), 'message': error.msg,
             'headers': str(error.hdrs), 'url': error.url}, True)
    except urllib.error.URLError as error:
        error_output_as_json(
            {'type': 'URLError', 'reason': str(error.reason), 'url': url}, True)


def test_getweb_resource():
    """
    test
    """
    try:
        _wr = getweb_resource('https://thisworld.is')
    except:
        import traceback
        raise ValueError(
            'www.test_getweb_resource()',
            'initiation issue\n------------------\n' +
            traceback.format_exc() + '------------------')


def load_pickle(file_name):
    """
    load a picle on the local os, and return data as dictionary
    """
    import os
    import pickle

    if os.path.exists(file_name):
        file = open(file_name, 'rb')
        return_pickle = pickle.load(file)
        file.close()
        return return_pickle

    return None


def test_load_pickle():
    """
    test
    """
    try:
        import os
        this_dir = os.path.dirname(os.path.realpath(__file__))
        # should give a result on dev and docker environments
        pick = load_pickle(this_dir + '/../../database/used_by_testdrive.pickle')
        # in beta and prod the database folder is located differently
        if not pick:
            pick = load_pickle(this_dir + '/../database/used_by_testdrive.pickle')
    except:
        import traceback
        raise ValueError(
            'www.test_load_pickle()',
            'initiation issue\n------------------\n' +
            traceback.format_exc() + '------------------')
    if not pick:
        raise ValueError(
            'www.test_load_pickle()',
            'and the addition to the list has been flowered by blue dripping liquid.')
    if not pick['hello'] == 'world!':
        raise ValueError(
            'www.test_load_pickle()',
            'the butterfly has lost its wings. the air\'s too thick to breathe.')


def load_rss_feed(url_list):
    """
    go through the list of urls and load the feed
    """
    import feedparser

    # prevent feedparser from filtering <iframe> and HTML5 elements
    # pylint: disable=protected-access
    feedparser._HTMLSanitizer.acceptable_elements.add('iframe')
    feedparser._HTMLSanitizer.acceptable_elements.add('article')
    feedparser._HTMLSanitizer.acceptable_elements.add('section')
    feedparser._HTMLSanitizer.acceptable_elements.add('header')
    feedparser._HTMLSanitizer.acceptable_elements.add('footer')
    feedparser._HTMLSanitizer.acceptable_elements.add('aside')
    feedparser._HTMLSanitizer.acceptable_elements.add('audio')
    feedparser._HTMLSanitizer.acceptable_elements.add('video')

    return_feed = {}

    for url in url_list:
        # load the feed
        feed = getweb_resource(url)
        feed = feedparser.parse(feed)

        # either add new entries to existing feed, or store a new feed
        if return_feed:
            return_feed['entries'] = return_feed['entries'] + feed.entries
        else:
            return_feed = feed

    return return_feed


def test_load_rss_feed():
    """
    test
    """
    try:
        _out_feed = load_rss_feed(['https://www.nasa.gov/rss/dyn/lg_image_of_the_day.rss'])
    except:
        import traceback
        raise ValueError(
            'www.test_load_rss_feed()',
            'initiation issue\n------------------\n' +
            traceback.format_exc() + '------------------')


def asciify(text):
    """
    replace special characters with ascii code
    https://stackoverflow.com/questions/6116978/how-to-replace-multiple-substrings-of-a-string
    """
    from functools import reduce

    replacements = (('&aelig;', 'ae'), ('&oslash;', 'oe'), ('&aring;', 'aa'), (' ', ''), ('&', ''),
                    (';', ''), (',', ''), ('.', ''), ('(', ''), (')', ''), ('-', ''), ('\'', ''),
                    ('`', ''))

    return reduce(lambda a, kv: a.replace(*kv), replacements, text)


def test_asciify():
    """
    test
    """
    try:
        asciified = asciify('        &&&       &aelig;&oslash;&aring;')
    except:
        import traceback
        raise ValueError(
            'www.test_asciify()',
            'initiation issue\n------------------\n' +
            traceback.format_exc() + '------------------')
    if not asciified == 'aeoeaa':
        raise ValueError('www.test_asciify()', 'call the vikings!')


def url_encode(url):
    """
    encode
    """
    if url:
        return str(url).replace('+', '%2B').replace('/', '%2F').replace(' ', '%20')

    return None


def test_url_encode():
    """
    test
    """
    try:
        encoded_url = url_encode('www.thisworld.is/awesome+real')
    except:
        import traceback
        raise ValueError(
            'www.test_url_encode()',
            'initiation issue\n------------------\n' +
            traceback.format_exc() + '------------------')
    if not encoded_url == 'www.thisworld.is%2Fawesome%2Breal':
        raise ValueError('www.test_url_encode()', 'change your attitiude, mate!')


def url_decode(url):
    """
    decode
    """
    if url:
        return url.replace('%2B', '+').replace('%2F', '/').replace('%20', ' ')

    return None


def test_url_decode():
    """
    test
    """
    try:
        decoded_url = url_decode('www.thisworld.is%2Fawesome%2Breal')
    except:
        import traceback
        raise ValueError(
            'test_url_decode()',
            'initiation issue\n------------------\n' +
            traceback.format_exc() + '------------------')
    if not decoded_url == 'www.thisworld.is/awesome+real':
        raise ValueError('test_url_decode()', 'hey, ho, santa is coming!')


def html_to_text(html_code):
    """
    http://love-python.blogspot.com/2011/04/html-to-text-in-python.html
    """
    import re

    text = html_code

    # remove the newlines
    text = text.replace('\n', ' ')
    text = text.replace('\r', ' ')

    # get only the body content
    #bodyPat = re.compile(r'< body[^<>]*?>(.*?)< / body >', re.I)
    #result = re.findall(bodyPat, data)
    #data = result[0]

    # replace html break tags with spaces
    text = text.replace('<br />', ' ').replace('<br/>', ' ').replace('<br>', ' ')

    # remove the java script
    regexp_object = re.compile(r'< script[^<>]*?>.*?< / script >')
    text = regexp_object.sub('', text)

    # remove the css styles
    regexp_object = re.compile(r'< style[^<>]*?>.*?< / style >')
    text = regexp_object.sub('', text)

    # remove html comments
    regexp_object = re.compile(r'')
    text = regexp_object.sub('', text)

    # remove all the html tags
    regexp_object = re.compile(r'<[^<]*?>')
    text = regexp_object.sub('', text)

    # replace consecutive spaces into a single one
    text = ' '.join(text.split())

    return text

def test_html_to_text():
    """
    test
    """
    try:
        text = html_to_text('give me <a> <br><a href="https://thisworld.is">thisworld.is</a>      ')
    except:
        import traceback
        raise ValueError(
            'www.test_html_to_text()',
            'initiation issue\n------------------\n' +
            traceback.format_exc() + '------------------')
    if text != 'give me thisworld.is':
        raise ValueError('www.test_html_to_text()', 'then, what do you want?')
