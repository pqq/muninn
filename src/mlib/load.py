# 18.11.08 // frode burdal klevstul // www.thisworld.is

"""
load data from a source, like from cache (pickle), rss or gitlab.
"""

def _get_feed_type(source_path):
    """
    determines the feed type based on the path to the source
    ("private" function)
    """
    feed_type = ''
    if source_path.endswith(".json"):
        feed_type = 'json'
    elif source_path.startswith('gitlab:'):
        # the gitlab module returns a feed of type json
        feed_type = 'json'
    elif source_path == 'huginn':
        feed_type = None
    else:
        feed_type = 'rss'
    return feed_type


def _get_cache(db_data, feed):
    """
    try to load cached data from a pickle on the os
    ("private" function)
    """
    from mlib.www import load_pickle as www_load_pickle
    from mlib.www import load_rss_feed as www_load_rss_feed

    if (
            db_data.parameters['cache'] == 'true'
            and not db_data.feed_source[0].startswith('gitlab:')
            and not db_data.feed_source[0] == 'huginn'
        ):

        feed = www_load_pickle(db_data.filename_feed)
        if feed:
            cache_use_status = 'true'
        else:
            feed = www_load_rss_feed(db_data.feed_source)
            cache_use_status = 'false (cache not found)'
    else:
        cache_use_status = 'false'
    return feed, cache_use_status


def data(
        db_data
        ):
    """
    load muninn readable content
    """
    import time
    from mlib.conversion import json_to_dictionary as conversion_json_to_dictionary
    from mlib.debug import add_to_debug_dict as debug_add_to_debug_dict
    from mlib.gitlab import get_data as gitlab_get_data
    from mlib.version import get as version_get
    from mlib.www import getweb_resource as www_getweb_resource
    from mlib.www import load_rss_feed as www_load_rss_feed

    start_time = time.time()
    feed_type = ''
    cache_use_status = ''
    feed = {}
    feed_type = _get_feed_type(db_data.feed_source[0])
    feed, cache_use_status = _get_cache(db_data, feed)

    # direct loading of resource
    if 'false' in cache_use_status:

        # if the feed is a json file
        #   note: we do not support bundled json files. hence we only use the
        #   first list element
        if db_data.feed_source[0].endswith(".json"):
            json_data = www_getweb_resource(db_data.feed_source[0])
            feed = conversion_json_to_dictionary(json_data)

        # if feed is a gitlab repository
        elif db_data.feed_source[0].startswith('gitlab:'):

            # the gitlab feed source is on the format 'gitlab:project_name'
            feed = gitlab_get_data(
                db_data=db_data,
                project=db_data.feed_source[0][7:],
                file_id=db_data.parameters['file_id'],
                path=db_data.parameters['path']
                )

        # if feed is a huginn bundle
        elif db_data.feed_source[0] == 'huginn':
            feed['huginn'] = db_data.huginn

        # in all other cases the feed has to be of type rss (otherwise we'll
        # run into problems)
        else:
            feed = www_load_rss_feed(db_data.feed_source)

    # store version version details
    feed['version_details'] = version_get()

    # store the parameters in the feed, so they can be used/presented later on
    feed['parameters'] = db_data.parameters

    # store the actual cache use status, instead of given cache value.
    # so, the result will contain the status telling whether or not
    # cache usage was successfully used
    feed['cache_use_status'] = cache_use_status

    # store the feed type, for later use
    if feed_type:
        feed['feed_type'] = feed_type

    # store the number of seconds it took to load the feed
    end_time = time.time()
    feed['time_feed_loading'] = end_time - start_time

    # in gitlab_get_data() a debug_object is added to feed, so we might
    # have to pass it on to preserve debug values
    if 'debug_object' in feed:
        feed['debug_object'] = debug_add_to_debug_dict(
            {
                'feed_source': db_data.feed_source,
                'feed_type': feed_type
            },
            feed['debug_object']
            )
    # if not, create a new debug object in the feed dict
    else:
        feed['debug_object'] = debug_add_to_debug_dict(
            {
                'feed_source': db_data.feed_source,
                'feed_type': feed_type
            }
            )

    return feed


def test_load():
    """
    test
    """
    from mlib.database import Database
    from mlib.parameters import get as parameters_get
    parameters = parameters_get()
    parameters['bundle_id'] = None # in case default value 'huginn' is used for testing
    database = Database(parameters)
    try:
        feed = data(database.data)
    except:
        import traceback
        raise ValueError(
            'load.test_load()',
            'load issue\n------------------\n'
            + traceback.format_exc()
            + '------------------'
            )
    if not 'cache_use_status' in feed:
        raise ValueError('load.test_load()', 'cache?! what cache?')
    if feed['cache_use_status'] == 'true':
        try:
            _the_entries = feed['entries']
        except:
            import traceback
            raise ValueError(
                'load.test_load()',
                'they put a man on the moon.'
                + ' at least, that is what they do say.\n------------------\n'
                + traceback.format_exc()
                + '------------------'
                )
    else:
        try:
            _the_title = feed['feed']['title']
        except:
            import traceback
            raise ValueError(
                'load.test_load()',
                'the title that had issues and lead to an raise,'
                + ' not in salary, but in time needed for debugging'
                + '\n------------------\n'
                + traceback.format_exc()
                + '------------------'
                )
