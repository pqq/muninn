# 18.11.15 // frode burdal klevstul // www.thisworld.is

"""
markdown related functions
"""

def to_html(markd):
    """
    convert muninn-style-markdown to html
    using https://github.com/Python-Markdown/markdown
    """
    import markdown
    html = markdown.markdown(
        markd, extensions=[
            'markdown.extensions.tables',
            'markdown.extensions.codehilite',
            'markdown.extensions.fenced_code',
            'markdown.extensions.smarty',
            'markdown.extensions.nl2br'
            ])
    return html


def test_to_html():
    """
    test
    """
    markd = '# t1\n\n### t3\nEOF'
    html = to_html(markd)
    if not '<h1>t1</h1>\n<h3>t3</h3>\n<p>EOF</p>' in html:
        raise ValueError('markdown.test_toHtml()', 'conversion issue')
