# 19.03.07 // frode burdal klevstul // www.thisworld.is

"""
data used by testing functions
"""

def get_test_entries():
    """
    return entries for testing
    """
    return [
        {
            'entry_content_html': '<body one>',
            'entry_content_text': 'body one',
            'entry_id':1,
            'entry_link': 'thisworld.is',
            'entry_published_date_display': '',
            'entry_tags': ['onetone', 'onettwo'],
            'entry_title': 'title one',
            'feed_country_code': 'jap',
            'feed_id': 'fone',
            'feed_main_category_id': 'maincat1',
            'feed_main_category': 'main cat 1',
            'feed_profile_id': '180876',
            'feed_source_id': 'fsione',
            'feed_source': 'fsi one',
            'feed_special_code': '',
            'feed_sub_category_id': 'subcat1',
            'feed_sub_category': 'sub cat 1'
        },
        {
            'entry_content_html': '<body two>',
            'entry_content_text': 'body two',
            'entry_id': 2,
            'entry_link': 'thisworld.is',
            'entry_published_date_display': '',
            'entry_tags': ['twotone', 'twottwo'],
            'entry_title': 'title two',
            'feed_country_code': 'nor',
            'feed_id': 'fone',
            'feed_main_category_id': 'maincat2',
            'feed_main_category': 'main cat 2',
            'feed_profile_id': '180876',
            'feed_source': 'fsi two',
            'feed_source_id': 'fsitwo',
            'feed_special_code': '',
            'feed_sub_category_id': 'subcat2',
            'feed_sub_category': 'sub cat 2'
        }
    ]
