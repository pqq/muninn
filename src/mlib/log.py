# 18.10.29 // frode burdal klevstul // www.thisworld.is

"""
logging
"""

def request(db_data):
    """
    log the request
    """
    import os
    import json
    import datetime
    from mlib.error import output_as_json as error_output_as_json

    timestamp = datetime.datetime.now().strftime('%Y%m%d%H%M%S')

    log_data = {
        timestamp: {
            'REQUEST_URI':
                (os.environ['REQUEST_URI'] if 'REQUEST_URI' in os.environ else ''),
            'REDIRECT_STATUS':
                (os.environ['REDIRECT_STATUS'] if 'REDIRECT_STATUS' in os.environ else ''),
            'REQUEST_METHOD':
                (os.environ['REQUEST_METHOD'] if 'REQUEST_METHOD' in os.environ else ''),
            'SERVER_PROTOCOL':
                (os.environ['SERVER_PROTOCOL'] if 'SERVER_PROTOCOL' in os.environ else ''),
            'SERVER_PORT':
                (os.environ['SERVER_PORT'] if 'SERVER_PORT' in os.environ else ''),
            'REMOTE_ADDR':
                (os.environ['REMOTE_ADDR'] if 'REMOTE_ADDR' in os.environ else ''),
            'REMOTE_PORT':
                (os.environ['REMOTE_PORT'] if 'REMOTE_PORT' in os.environ else ''),
            'HTTP_HOST':
                (os.environ['HTTP_HOST'] if 'HTTP_HOST' in os.environ else ''),
            'HTTP_USER_AGENT':
                (os.environ['HTTP_USER_AGENT'] if 'HTTP_USER_AGENT' in os.environ else ''),
            'HTTP_ACCEPT':
                (os.environ['HTTP_ACCEPT'] if 'HTTP_ACCEPT' in os.environ else ''),
            'HTTP_ACCEPT_LANGUAGE':
                (
                    os.environ['HTTP_ACCEPT_LANGUAGE']
                    if 'HTTP_ACCEPT_LANGUAGE' in os.environ else ''
                ),
            'HTTP_ACCEPT_ENCODING':
                (
                    os.environ['HTTP_ACCEPT_ENCODING']
                    if 'HTTP_ACCEPT_ENCODING' in os.environ else ''
                ),
            'HTTP_DNT':
                (
                    os.environ['HTTP_DNT']
                    if 'HTTP_DNT' in os.environ else ''
                ),
            'HTTP_CONNECTION':
                (
                    os.environ['HTTP_CONNECTION']
                    if 'HTTP_CONNECTION' in os.environ else ''
                ),
            'HTTP_UPGRADE_INSECURE_REQUESTS':
                (
                    os.environ['HTTP_UPGRADE_INSECURE_REQUESTS']
                    if 'HTTP_UPGRADE_INSECURE_REQUESTS' in os.environ else ''
                ),
            'HTTP_CACHE_CONTROL':
                (
                    os.environ['HTTP_CACHE_CONTROL']
                    if 'HTTP_CACHE_CONTROL' in os.environ else ''
                )
        }
    }

    if 'REQUEST_URI' in os.environ:
        if os.path.exists(db_data.log):
            try:
                file = open(db_data.log, 'a')
            except (OSError, IOError):
                error_output_as_json(
                    'log: unable append data to the logfile [' + db_data.log + ']', True)
        else:
            try:
                file = open(db_data.log, 'w')
            except (OSError, IOError):
                error_output_as_json(
                    'log: unable create a new logfile with the path [' + db_data.log + '] - ' +
                    ' possibly due to insufficient access rights or wrong path', True)

        file.write(json.dumps(log_data, sort_keys=True, indent=4, separators=(',', ': ')))
        file.write(',\n')
        file.close()


def test_request():
    """
    test
    """
    try:
        request(None)
    except:
        import traceback
        raise ValueError(
            'log.test_request()',
            'initiation issue\n------------------\n' +
            traceback.format_exc() + '------------------')
