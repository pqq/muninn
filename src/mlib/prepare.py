# 18.11.08 // frode burdal klevstul // www.thisworld.is

"""
prepares data to ensure data correctness
"""

# comment on code quality and disabling pylint checks:
#     this is "legacy code" from before using pylint for checking code quality.
#     the code fails on
#         - too many local variables
#         - too many branches
#         - too many statements
#     instead of doing a complete rewrite of i've simply added pylint exceptions to
#     disable the relevant checks.
#
# pylint: disable=too-many-locals, too-many-branches, too-many-statements

def content_data(db_data, feed_data):
    """
    prepare the feed data's dictionary, so it is on the correct format. note that we assume that all
    dictionaries of type 'json' are assumed to be correct, and hence just returned. at a later
    stage, if needed, we can implement checking and fixing of json content as well. this would be
    the place for that logic.
    """
    import hashlib
    from mlib.conversion import pub_date_to_display_date as conversion_pub_date_to_display_date
    from mlib.datastructure import Entry, Feed
    from mlib.error import output_as_json as error_output_as_json
    from mlib.error import return_as_entries as error_return_as_entries
    from mlib.extract import feed_title_elements as extract_feed_title_elements
    from mlib.www import html_to_text as www_html_to_text

    return_feed = {}

    # if the feed is of type json
    if feed_data['feed_type'] == 'json':

        # remove 'feed_type' pass on the rest of the data
        del feed_data['feed_type']

        return_feed = feed_data

    # the rss feed has to be formatted / refined
    elif feed_data['feed_type'] == 'rss':
        hits = 0
        entries = []

        # loop through all entries, in the default rss formatted feed
        for entry in feed_data['entries']:
            hits = hits + 1

            if not 'source' in entry:
                entry['source'] = {}

            # if a title is set in the feed json file it is stored in db_data; that title overwrites
            # the title given in entry source. note: an empty sequence (string) returns false.
            if hasattr(db_data, 'feed_title') and db_data.feed_title:
                entry['source']['title'] = db_data.feed_title

            # ids differ in between plain rss and inoreader's rss (ref #437 #451)
            # inoreader id example:
            #   <guid isPermaLink="false">http://www.inoreader.com/article/3a9c6e7eb15eb0b8</guid>
            if 'inoreader.com' in entry['id']:
                this_entry_id = entry['id'].rsplit('/', 1)[1]
            # blogger.com id examples:
            #   <id>tag:blogger.com,1999:blog-1950818968274202234</id>
            #   <id>tag:blogger.com,1999:blog-1950818968274202234.post-8049782550801938373</id>
            elif 'blogger.com' in entry['id']:
                this_entry_id = entry['id'].rpartition('-')[2]
            # youtube.com id example:
            #   <id>yt:video:_nXUzwUDeHE</id>
            elif 'yt:video' in entry['id']:
                this_entry_id = entry['id'].rpartition(':')[2]
            # for all other ids we hash the id string into a unique integer
            else:
                this_entry_id = str(int(hashlib.md5(entry['id'].encode('utf-8')).hexdigest(), 16))

            entry_title = entry['title_detail'].value

            # content is stored differently in plain rss than in inoreader's rss (ref #437)
            if 'summary_detail' in entry:
                content_html = entry['summary_detail'].value
            elif 'content' in entry:
                content_html = entry['content'][0].value
            else:
                error_output_as_json('content not found in entry', True)

            entry_link = entry['link']
            entry_published_date = entry['published']

            # add tags
            # ex:
            # 'tags': [{'label': None, 'scheme': None, 'term': u'jernviljeNo-prod'},
            #          {'label': None, 'scheme': None, 'term': u'jernviljeNo-dev'},
            #          {'label': None, 'scheme': None, 'term': u'jernviljeNo-beta'}]
            entry_tags = []
            if 'tags' in entry:
                for tag in entry['tags']:
                    entry_tags.append(tag['term'])

            if not 'title' in entry['source']:
                error_output_as_json('title is missing from feed', True)

            # get elements from a special crafted title. the format of the title of the feed has to
            # be exactly on the format:
            # "profile | countrycode | maincategory | subcategory | source | specialcode | feedid"
            feed_title_elements = extract_feed_title_elements(entry['source']['title'])

            new_entry = Entry()
            Entry().setattribute(new_entry, 'entry_content_html', content_html)
            Entry().setattribute(new_entry, 'entry_content_text', www_html_to_text(content_html))
            Entry().setattribute(new_entry, 'entry_id', this_entry_id)
            Entry().setattribute(new_entry, 'entry_link', entry_link)
            Entry().setattribute(new_entry, 'entry_number', hits)
            Entry().setattribute(new_entry, 'entry_published_date', entry_published_date)
            Entry().setattribute(
                new_entry,
                'entry_published_date_display',
                conversion_pub_date_to_display_date(entry_published_date, 'display')
            )
            Entry().setattribute(
                new_entry,
                'entry_published_date_nerd',
                conversion_pub_date_to_display_date(entry_published_date, 'nerd')
            )
            Entry().setattribute(new_entry, 'entry_tags', entry_tags)
            Entry().setattribute(new_entry, 'entry_title', entry_title)
            Entry().setattribute(
                new_entry,
                'feed_country_code',
                feed_title_elements['feed_country_code']
            )
            Entry().setattribute(new_entry, 'feed_id', feed_title_elements['feed_id'])
            Entry().setattribute(
                new_entry,
                'feed_main_category',
                feed_title_elements['feed_main_category']
            )
            Entry().setattribute(
                new_entry,
                'feed_main_category_id',
                feed_title_elements['feed_main_category_id']
            )
            Entry().setattribute(new_entry, 'feed_profile', feed_title_elements['feed_profile'])
            Entry().setattribute(
                new_entry,
                'feed_profile_id',
                feed_title_elements['feed_profile_id']
            )
            Entry().setattribute(new_entry, 'feed_source', feed_title_elements['feed_source'])
            Entry().setattribute(new_entry, 'feed_source_id', feed_title_elements['feed_source_id'])
            Entry().setattribute(
                new_entry,
                'feed_special_code',
                feed_title_elements['feed_special_code']
            )
            Entry().setattribute(
                new_entry,
                'feed_sub_category',
                feed_title_elements['feed_sub_category']
            )
            Entry().setattribute(
                new_entry,
                'feed_sub_category_id',
                feed_title_elements['feed_sub_category_id']
            )

            entries.append(new_entry.unload())

        # note that we do not want to loop all fields in feed ('for i in feed') as feedparser has
        # added loads of additional fields that don't want to present in the json output
        return_feed['cache_use_status'] = feed_data['cache_use_status']
        return_feed['hits'] = hits
        return_feed['parameters'] = feed_data['parameters']
        return_feed['entries'] = entries
        return_feed['time_feed_loading'] = feed_data['time_feed_loading']
        if 'time_total' in feed_data:
            return_feed['time_total'] = feed_data['time_total']
        return_feed['version_details'] = feed_data['version_details']

        return_feed['debug_object'] = feed_data['debug_object']
        return_feed['debug_object']['hits_initial'] = hits

    else:
        return_feed['entries'] = error_return_as_entries(
            'unknown feed_type ['+feed_data['feed_type']+']')

    # ensure data structure correctness
    feed_obj = Feed()
    feed_obj.forceload(return_feed)
    if not Feed().validate(feed_obj):
        error_output_as_json('prepare : unknown attributes in the feed data structure', True)

    return return_feed


def test_content_data():
    """
    test
    """
    feed_data = {
        'feed_type': 'json',
        'entries': [
            {'entry_id':1},
            {'entry_id':2}
        ]
    }
    try:
        db_data = lambda: None
        db_data.feed_title = (
            'fbk |  eng | maincat | subcat | gitlab | ff=1 gts=0 ws=1 pli=0 edd=1 | 1812162142')
        out_feed = content_data(db_data, feed_data)
    except:
        import traceback
        raise ValueError(
            'prepare.test_content_data()',
            'initiation issue\n------------------\n' +
            traceback.format_exc() + '------------------')
    if not out_feed:
        raise ValueError('prepare.test_content_data()', 'when you do not get what you give')
    try:
        from mlib.load import data as load_data
        from mlib.database import Database
        database = Database(
            {'service_id': 'atestservice', 'bundle_id': 'fno', 'cache': 'false', 'debug': 'false'})
        feed = load_data(database.data)
        feed = content_data(db_data, feed)
    except:
        import traceback
        raise ValueError(
            'prepare.test_content_data()',
            'well, several issues might have caused this\n------------------\n' +
            traceback.format_exc() + '------------------')
