# 18.11.05 // frode burdal klevstul // www.thisworld.is

"""
the database class stores all information that is loaded from database.json
"""

# pylint: disable=too-few-public-methods
class Database:
    """
    database class
    """

    data = None


    def __init__(self, parameters=None):
        #
        # initiates a database with content
        #
        self.data = self.Data()

        if parameters:
            self.load_data(parameters)
        # else: the initiateDatabase() function has to be called after
        #       creating a new Database instance, if needed


    # pylint: disable=too-many-instance-attributes
    class Data:
        """
        used for storing the data in the database
        """

        def __init__(self):
            self.banned_body_content = []
            self.banned_entries = []
            self.banned_title_content = []
            self.base_url = ''
            self.core = ''
            self.feed_source = ''
            self.feed_title = ''
            self.filename_feed = ''
            self.huginn = ''
            self.loaded = False
            self.log = ''
            self.parameters = ''
            self.setup_url = ''
            self.sources = ''


    def __load_banned_data(self, base_database_path):
        """
        load banned data
        """

        banned_data = {
            'banned_entries': [],
            "banned_title_content": [],
            "banned_body_content": []
        }

        # load banned entries
        file_name = base_database_path + '/bannedEntries.txt'
        with open(file_name) as file:
            list_of_lines = file.read().splitlines()

            for line in list_of_lines:
                if not line.startswith('#'):
                    line = line.split('#', 1)[0].rstrip()
                    banned_data['banned_entries'].append(line)
        file.close()

        # load banned title content
        self.data.banned_title_content = []
        file_name = base_database_path + '/bannedTitleContent.txt'
        with open(file_name) as file:
            list_of_lines = file.read().splitlines()

            for line in list_of_lines:
                if not line.startswith('#'):
                    line = line.split('#', 1)[0].rstrip()
                    banned_data['banned_title_content'].append(line)
        file.close()

        # load banned body content
        self.data.banned_body_content = []
        file_name = base_database_path + '/bannedBodyContent.txt'
        with open(file_name) as file:
            list_of_lines = file.read().splitlines()

            for line in list_of_lines:
                if not line.startswith('#'):
                    line = line.split('#', 1)[0].rstrip()
                    banned_data['banned_body_content'].append(line)
        file.close()

        return banned_data


    @staticmethod
    def __verify_database_content(db_content, service_id, env_id):
        """
        verify that the database content is all good
        https://stackoverflow.com/questions/12179271/meaning-of-classmethod-and-staticmethod-for-beginner
        """
        from mlib.error import output_as_json as error_output_as_json

        # verfy that the the given service_id is in the database
        if not service_id in db_content:
            error_output_as_json('database: no data for service_id [' + service_id + ']', True)

        # verify that there is a log entry
        if not 'log' in db_content[service_id]:
            error_output_as_json('database: no log entry for service_id [' + service_id + ']', True)

        # verify that there is a log status
        if not 'status' in db_content[service_id]['log']:
            error_output_as_json('database: no log status for service_id [' + service_id +
                                 ']', True)

        # verify that there is a log entry for given environment
        if not env_id in db_content[service_id]['log']:
            error_output_as_json('database: no log entry for service_id [' + service_id +
                                 '] and env_id [' + env_id + ']', True)

        # check that the log file has the correct ending
        if not (
                db_content[service_id]['log'][env_id].endswith('.txt') or
                db_content[service_id]['log'][env_id].endswith('.log')
            ):
            error_output_as_json('database: unknown file type for log file [' +
                                 db_content[service_id]['log'][env_id] + ']', True)

        # verfiy the existience of baseurl
        if not 'baseurl' in db_content[service_id]:
            error_output_as_json('database: no base url entry for service_id [' +
                                 service_id + ']', True)

        # verify the existence of given environment in baseurl
        if not env_id in db_content[service_id]['baseurl']:
            error_output_as_json('database: no base url entry for service_id [' + service_id +
                                 '] and env_id [' + env_id + ']', True)

        # verfiy the existience of setupurl
        if not 'setupurl' in db_content[service_id]:
            error_output_as_json('database: no setup url entry for service_id [' +
                                 service_id + ']', True)

        # verify the existence of given environment in setupurl
        if not env_id in db_content[service_id]['setupurl']:
            error_output_as_json('database: no setup url entry for service_id [' + service_id +
                                 '] and env_id [' + env_id + ']', True)


    @staticmethod
    def __verify_setup_data(setup_data, env_id, setup_url):
        """
        verify that the setup data is all good
        """
        from mlib.error import output_as_json as error_output_as_json

        if not 'core' in setup_data:
            error_output_as_json('database: no core in setup data at ['+setup_url+']', True)

        if not env_id in setup_data['core']:
            if not 'default' in setup_data['core']:
                error_output_as_json('database: no core data for env_id [' + env_id + ']', True)

        if not 'huginn' in setup_data:
            error_output_as_json('database: no huginn in setup data', True)

        if not env_id in setup_data['huginn']:
            if not 'default' in setup_data['huginn']:
                error_output_as_json('database: no huginn data for env_id [' + env_id + ']', True)

        if not 'sources' in setup_data:
            error_output_as_json('database: no sources in setup data', True)


    @staticmethod
    def __get_feed_source(bundle_id, env_id, sources):
        """
        get a specific feed source
        """
        from mlib.error import output_as_json as error_output_as_json

        feed_source = ''

        # build a string with 'src', bundle_id and env_id
        # https://stackoverflow.com/questions/8626694/join-4-strings-to-the-one-if-they-are-not-empty-in-python
        src_bundle_env_id = '.'.join(filter(None, ['src', bundle_id, env_id]))

        try:
            feed_source = sources[src_bundle_env_id]
        except KeyError:
            try:
                # in case loading of the environment specific feed source failed
                # we try the default one
                default_feed_source = src_bundle_env_id[:src_bundle_env_id.rfind('.')] + '.default'
                feed_source = sources[default_feed_source]
            except KeyError:
                error_output_as_json('database: could not find a feed source with the name [' +
                                     src_bundle_env_id + '] nor a default feed source named [' +
                                     default_feed_source + ']', True)

        return feed_source


    def load_data(self, parameters):
        """
        responsible for loading the data into the database
        """
        #
        # load database with data
        #
        import os
        import json
        from mlib.conversion import json_to_dictionary as conversion_json_to_dictionary
        from mlib.environment import get as environment_get
        from mlib.www import getweb_resource as www_getweb_resource

        # value used for checking status of database initialisation
        self.data.loaded = True

        # store parameters for usage later on
        self.data.parameters = parameters

        # get the environment id
        env_id = environment_get()

        # get the path to this current file, and locate the database directory from here
        base_database_path = os.path.dirname(os.path.realpath(__file__)) + '/../../database'

        # load the database file
        file = open(base_database_path +'/database.json', 'r')
        db_content = json.load(file)
        file.close()

        # verify that the database data is valid
        self.__verify_database_content(db_content, parameters['service_id'], env_id)

        # get the log file path if log status is turned on
        if db_content[parameters['service_id']]['log']['status'] == 'on':
            self.data.log = db_content[parameters['service_id']]['log'][env_id]

        # get the service base_url - this value is (currently) only used when
        # outputting the rss feed
        self.data.base_url = db_content[parameters['service_id']]['baseurl'][env_id]

        # get the setup url
        self.data.setup_url = db_content[parameters['service_id']]['setupurl'][env_id]

        # load setup file
        setup_data = conversion_json_to_dictionary(www_getweb_resource(self.data.setup_url))

        # verify that the setup data is valid
        self.__verify_setup_data(setup_data, env_id, self.data.setup_url)

        # get core data from the setup file
        if env_id in setup_data['core']:
            self.data.core = setup_data['core'][env_id]
        else:
            # get default data if environment specific data was not found
            self.data.core = setup_data['core']['default']

        # get huginn data from the setup file
        if env_id in setup_data['huginn']:
            self.data.huginn = setup_data['huginn'][env_id]
        else:
            # get default data if environment specific data was not found
            self.data.huginn = setup_data['huginn']['default']

        # get sources from the setup file
        self.data.sources = setup_data['sources']

        # spacial case where bundle_id == huginn - used to output huginn specific data
        if parameters['bundle_id'] == 'huginn':
            self.data.feed_source = ['huginn']
            return

        self.data.feed_source = self.__get_feed_source(parameters['bundle_id'],
                                                       env_id, self.data.sources)

        # process feed_source
        #   example formats found in:
        #       https://gitlab.com/klevstul/muninn/blob/master/setup/setup.atestservice.json
        if 'title={' in self.data.feed_source:
            self.data.feed_title = self.data.feed_source.split(' title={')[1][:-1]

            # bundle format example:
            #   "src.fno.default": "bundle={http://url1.com, https://url2.com} title={...}"
            if 'bundle={' in self.data.feed_source:
                self.data.feed_source = self.data.feed_source.replace(' ', '', 999)
                self.data.feed_source = self.data.feed_source.split('bundle={')[1].split('}')[0]
                self.data.feed_source = self.data.feed_source.split(',')
            else:
                self.data.feed_source = [self.data.feed_source.split(' title={')[0].rstrip()]

        else:
            self.data.feed_title = ''
            self.data.feed_source = [self.data.feed_source]

        banned_data = self.__load_banned_data(base_database_path)
        self.data.banned_entries = banned_data['banned_entries']
        self.data.banned_title_content = banned_data['banned_title_content']
        self.data.banned_body_content = banned_data['banned_body_content']

        # set path to pickle file
        self.data.filename_feed = base_database_path + '/cache.' + '.'.join(filter(
            None, [parameters['service_id'], parameters['bundle_id'], env_id])) + '.pickle'


def test_database():
    """
    test
    """
    from mlib.parameters import get as parameters_get
    parameters = parameters_get()
    parameters['bundle_id'] = None # in case 'huginn' is set as default parameter for testing
    database = None
    try:
        database = Database(parameters)
    except:
        import traceback
        raise ValueError(
            'test_database()',
            'initiation of database failed\n------------------\n' +
            traceback.format_exc() + '------------------')
    try:
        # variable starting with underscore to avoide "unused variable" in pylint
        # https://stackoverflow.com/questions/10107350/how-to-handle-the-pylint-message-idw0612-unused-variable
        _db_data = database.data
    except:
        import traceback
        raise ValueError(
            'test_database()',
            'database.data attribute not found\n------------------\n' +
            traceback.format_exc() + '------------------')
    try:
        no_attributes = 0
        for _attr, _value in database.data.__dict__.items():
            no_attributes += 1
        if no_attributes != 14:
            import traceback
            raise ValueError(
                'database.test_database()', str(no_attributes) +
                ' is a wrong number of attributes in database.data\n------------------\n' +
                traceback.format_exc() + '------------------')
        if not database.data.parameters:
            import traceback
            raise ValueError(
                'database.test_database()',
                'unable to look up database.data.parameters\n------------------\n' +
                traceback.format_exc() + '------------------')
        if not database.data.feed_source:
            import traceback
            raise ValueError(
                'database.test_database()',
                'unable to look up database.data.feed_source\n------------------\n' +
                traceback.format_exc() + '------------------')
    except ValueError as error:
        raise ValueError(error.args[0], error.args[1])
