# 18.11.11 // frode burdal klevstul // www.thisworld.is

"""
filter a feed
"""

# comment on code quality and disabling pylint checks:
#     this is "legacy code" from before using pylint for checking code quality. despite several
#     improvements being done the code fails on
#         - too many local variables
#         - too many branches
#         - too many statements
#     instead of doing a complete rewrite of filter.py i've simply added pylint exceptions to
#     disable the relevant checks. the code works as it is. if it becomes troublesome to keep
#     up-to-date i will consider a complete refactoring at a later stage in time.
#
# pylint: disable=too-many-locals, too-many-branches, too-many-statements

def __is_banned(db_data, entry, filter_debug_object):
    """
    check if an entry is banned
    """
    import json
    from mlib.debug import add_to_debug_dict as debug_add_to_debug_dict

    entry_is_banned = False

    # check if the entry id is banned
    if entry['entry_id'] in db_data.banned_entries:
        filter_debug_object = debug_add_to_debug_dict(
            {'entry_title': entry['entry_title'],
             'entry_published_date_display': entry['entry_published_date_display'],
             'feed_profile_id': entry['feed_profile_id']},
            filter_debug_object, 'banned_entry', entry['entry_id'])
        entry_is_banned = True

    # check if the content is banned unless the entry's feed is white listed (ws=1)
    if not 'ws=1' in entry['feed_special_code']:

        # check title
        for banned_string in db_data.banned_title_content:
            if (
                    entry['entry_title'].lower().find(banned_string.lower()) >= 0
                ):
                entry_is_banned = True
                filter_debug_object = debug_add_to_debug_dict(
                    {'entry_title': entry['entry_title'],
                     'entry_published_date_display': entry['entry_published_date_display'],
                     'feed_profile_id': entry['feed_profile_id'],
                     'banned_string': banned_string},
                    filter_debug_object, 'banned_title_content', entry['entry_id'])
                break

        # check body
        for banned_string in db_data.banned_body_content:
            if (
                    json.dumps(
                        entry['entry_content_text']).lower().find(banned_string.lower()) >= 0
                    or
                    json.dumps(
                        entry['entry_content_html']).lower().find(banned_string.lower()) >= 0
                ):
                entry_is_banned = True
                filter_debug_object = debug_add_to_debug_dict(
                    {'entry_title': entry['entry_title'],
                     'entry_published_date_display': entry['entry_published_date_display'],
                     'feed_profile_id': entry['feed_profile_id'],
                     'banned_string': banned_string},
                    filter_debug_object, 'banned_body_content', entry['entry_id'])
                break

    return entry_is_banned


def __is_fsf_mismatch(entry, filter_debug_object):
    """
    check entry for fsf (feed specific filter) mismatch
    """
    import json
    from mlib.debug import add_to_debug_dict as debug_add_to_debug_dict
    from mlib.extract import feed_special_code as extract_feed_special_code

    def __get_fsf_values(entry):
        """
        extract fsf values
        """
        # get the specified fsf parameter from the 'feed_special_code' value for the feed,
        # and split into include and exclude values
        fsf = extract_feed_special_code(entry['feed_special_code'], 'fsf')
        # replace underscore with space, as `_` represents space in the fsf value
        fsf = fsf.replace('_', ' ')
        # split fsf value on '-' into a list of values
        fsf_values = fsf.split('-')
        fsf_body_include = []
        fsf_body_exclude = []
        fsf_title_include = []
        fsf_title_exclude = []

        for fsf_value in fsf_values:
            if 'ib.' in fsf_value:
                fsf_body_include.append(fsf_value[fsf_value.find('.')+1:])
            elif 'eb.' in fsf_value:
                fsf_body_exclude.append(fsf_value[fsf_value.find('.')+1:])
            elif 'it.' in fsf_value:
                fsf_title_include.append(fsf_value[fsf_value.find('.')+1:])
            elif 'et.' in fsf_value:
                fsf_title_exclude.append(fsf_value[fsf_value.find('.')+1:])

        return {
            'fsf_body_include': fsf_body_include,
            'fsf_body_exclude': fsf_body_exclude,
            'fsf_title_include': fsf_title_include,
            'fsf_title_exclude': fsf_title_exclude
        }

    fsf_mismatch = False

    # if 'feed specific filter' (fsf) value is given as part of the 'feed_special_code'
    if 'fsf=' in entry['feed_special_code']:

        fsf_values = __get_fsf_values(entry)
        fsf = extract_feed_special_code(entry['feed_special_code'], 'fsf')

        # include body : makes sure feed's include strings exsit in the body
        for include_string in fsf_values['fsf_body_include']:
            if json.dumps(entry['entry_content_text']).lower().find(include_string) < 0:
                fsf_mismatch = True
                filter_debug_object = debug_add_to_debug_dict(
                    {'entry_title': entry['entry_title'],
                     'entry_published_date_display': entry['entry_published_date_display'],
                     'feed_profile_id': entry['feed_profile_id'],
                     'fsf_string': fsf,
                     'fsf_missing_include_string_from_body': include_string},
                    filter_debug_object, 'feed_specific_filter', entry['entry_id'])
                break

        # exclude body : makes sure feed's exclude strings DON'T exsit in the body
        if not fsf_mismatch:
            for exclude_string in fsf_values['fsf_body_exclude']:
                if json.dumps(entry['entry_content_text']).lower().find(exclude_string) >= 0:
                    fsf_mismatch = True
                    filter_debug_object = debug_add_to_debug_dict(
                        {'entry_title': entry['entry_title'],
                         'entry_published_date_display': entry['entry_published_date_display'],
                         'feed_profile_id': entry['feed_profile_id'],
                         'fsf_string': fsf,
                         'fsf_exclude_string_found_in_body': exclude_string},
                        filter_debug_object, 'feed_specific_filter', entry['entry_id'])
                    break

        # include title : makes sure feed's include strings exsit in the title
        if not fsf_mismatch:
            for include_string in fsf_values['fsf_title_include']:
                if entry['entry_title'].lower().find(include_string) < 0:
                    fsf_mismatch = True
                    filter_debug_object = debug_add_to_debug_dict(
                        {'entry_title': entry['entry_title'],
                         'entry_published_date_display': entry['entry_published_date_display'],
                         'feed_profile_id': entry['feed_profile_id'],
                         'fsf_string': fsf,
                         'fsf_missing_include_string_from_title': include_string},
                        filter_debug_object, 'feed_specific_filter', entry['entry_id'])
                    break

        # exclude title : makes sure feed's exclude strings DON'T exsit in the title
        if not fsf_mismatch:
            for exclude_string in fsf_values['fsf_title_exclude']:
                if entry['entry_title'].lower().find(exclude_string) >= 0:
                    fsf_mismatch = True
                    filter_debug_object = debug_add_to_debug_dict(
                        {'entry_title': entry['entry_title'],
                         'entry_published_date_display': entry['entry_published_date_display'],
                         'feed_profile_id': entry['feed_profile_id'],
                         'fsf_string': fsf,
                         'fsf_exclude_string_found_in_title': exclude_string},
                        filter_debug_object, 'feed_specific_filter', entry['entry_id'])
                    break

    return fsf_mismatch


def __is_filtered_by_field(filter_name, db_data, entry, filter_debug_object):
    """
    check if a given filter field name is set and matches the entry
    """
    from mlib.debug import add_to_debug_dict as debug_add_to_debug_dict

    filter_match = False

    if db_data.parameters[filter_name] is not None:
        if db_data.parameters[filter_name] != entry[filter_name]:
            filter_debug_object = debug_add_to_debug_dict(
                {'entry_title': entry['entry_title'],
                 'entry_published_date_display': entry['entry_published_date_display'],
                 'feed_profile_id': entry['feed_profile_id']},
                filter_debug_object, filter_name, entry['entry_id'])
            filter_match = True

    return filter_match


def __is_excluded(db_data, entry, filter_debug_object):
    """
    check if an exclude filter is set and if the entry matches
    """
    from mlib.debug import add_to_debug_dict as debug_add_to_debug_dict

    def __exclude_match_by_name(filter_name, filter_debug_object):
        """
        check for exclude match given a name
        """
        is_match = False

        if filter_name + '.' + entry[filter_name] in db_data.parameters['exclude']:
            filter_debug_object = debug_add_to_debug_dict(
                {'entry_title': entry['entry_title'],
                 'entry_published_date_display': entry['entry_published_date_display'],
                 'feed_profile_id': entry['feed_profile_id']},
                filter_debug_object, 'exclude.' + filter_name, entry['entry_id'])
            is_match = True

        return is_match

    filter_match = False

    # if an exclude filter is set
    if db_data.parameters['exclude'] is not None:

        filter_debug_object = debug_add_to_debug_dict(
            {'exclude_string': db_data.parameters['exclude']},
            filter_debug_object, 'exclude')

        if __exclude_match_by_name('feed_country_code', filter_debug_object):
            filter_match = True
        elif __exclude_match_by_name('feed_id', filter_debug_object):
            filter_match = True
        elif __exclude_match_by_name('feed_main_category_id', filter_debug_object):
            filter_match = True
        elif __exclude_match_by_name('feed_sub_category_id', filter_debug_object):
            filter_match = True
        elif __exclude_match_by_name('feed_profile_id', filter_debug_object):
            filter_match = True
        elif __exclude_match_by_name('feed_source_id', filter_debug_object):
            filter_match = True

    return filter_match


def __is_duped(db_data, entry, dupe_control, filter_debug_object):
    """
    check if dupe filter is set and we have a condition that leads to filtering
    """
    from mlib.debug import add_to_debug_dict as debug_add_to_debug_dict

    filter_match = False

    # if a dupe filter is set
    if db_data.parameters['avoid_dupes'] is not None:

        # debug info
        filter_debug_object = debug_add_to_debug_dict(
            {'avoid_string': db_data.parameters['avoid_dupes']},
            filter_debug_object, 'avoid_dupes')

        # title check:
        # check if we shall do a dupe control of entry titles
        if 'entry_title' in db_data.parameters['avoid_dupes']:
            # check if the entry title element has been added to our dupe_control dict
            if 'entry_title' in dupe_control:
                # check if current title (entry_title) has been read (and stored in
                # dupe_control) before
                if entry['entry_title'] in dupe_control['entry_title']:
                    filter_debug_object = debug_add_to_debug_dict(
                        {'entry_title': entry['entry_title'],
                         'entry_published_date_display': entry['entry_published_date_display'],
                         'feed_profile_id': entry['feed_profile_id'],
                         'dupe_hit': 'entry_title'},
                        filter_debug_object, 'avoid_dupes', entry['entry_id'])
                    # ... if it has, skip this one
                    filter_match = True
                else:
                    # ... if not add the title to our list of entry titles
                    dupe_control['entry_title'].append(entry['entry_title'])
                    # ... else if we do not have an entry_title element in our
                    #     dupe_control dict(ionary)
            else:
                # ... .... we add a element named 'entry_title', which relates to a list.
                #          this way we can create our own associative array / hash
                dupe_control.update({'entry_title' : []})
                # ... ... and we add the current title to the empty list of entry titles
                dupe_control['entry_title'].append(entry['entry_title'])

        # link check:
        # check if we shall do a dupe control of entry links
        if 'entry_link' in db_data.parameters['avoid_dupes']:
            if 'entry_link' in dupe_control:
                if entry['entry_link'] in dupe_control['entry_link']:
                    filter_debug_object = debug_add_to_debug_dict(
                        {'entry_title': entry['entry_title'],
                         'entry_published_date_display': entry['entry_published_date_display'],
                         'feed_profile_id': entry['feed_profile_id'],
                         'dupe_hit': 'entry_link'},
                        filter_debug_object, 'avoid_dupes', entry['entry_id'])
                    filter_match = True
                else:
                    dupe_control['entry_link'].append(entry['entry_link'])
            else:
                dupe_control.update({'entry_link' : []})
                dupe_control['entry_link'].append(entry['entry_link'])

    return filter_match


def __is_limited_by_count(field_name, db_data, entry, count_control, filter_debug_object):
    """
    check if a count filter is set and we have a condition that leads to filtering
    (limit how many entries per field_name to be presented)
    note that the input parameter has to be in the format
    (where "cl" is an abbreviation for "count limit"):
    'cl_[field_name]'
    """
    from mlib.debug import add_to_debug_dict as debug_add_to_debug_dict

    count_limit_reached = False

    if db_data.parameters['cl_' + field_name] is not None:
        if entry[field_name] in count_control:
            if count_control[entry[field_name]] < int(db_data.parameters['cl_' + field_name]):
                count_control[entry[field_name]] = count_control[entry[field_name]] + 1
            else:
                filter_debug_object = debug_add_to_debug_dict(
                    {'entry_title': entry['entry_title'],
                     'entry_published_date_display': entry['entry_published_date_display'],
                     'feed_profile_id': entry['feed_profile_id'],
                     'count_limit_reached': db_data.parameters['cl_' + field_name]},
                    filter_debug_object, field_name, entry['entry_id'])
                count_limit_reached = True
        else:
            count_control[entry[field_name]] = 1

    return count_limit_reached


def __is_filtered_by_tag(db_data, entry, filter_debug_object):
    """
    check if filter is set to filter on tags and we have a condition that leads to filtering
    here we keep the tags that are specified in the "entry_tags" parameter
    """
    from mlib.debug import add_to_debug_dict as debug_add_to_debug_dict

    # default true because if no parameter "entry_tags" is used we shall not filter away any entries
    tag_match = True

    # if we shall filter on tags
    if db_data.parameters['entry_tags'] is not None:
        tag_match = False
        entry_tags = db_data.parameters['entry_tags'].split('-')
        for tag in entry['entry_tags']:
            if tag in entry_tags:
                tag_match = True
                break

        if not tag_match:
            filter_debug_object = debug_add_to_debug_dict(
                {'entry_title': entry['entry_title'],
                 'entry_published_date_display': entry['entry_published_date_display'],
                 'feed_profile_id': entry['feed_profile_id'],
                 'entry_tags': entry['entry_tags']},
                filter_debug_object, 'entry_tags', entry['entry_id'])

    # filter functions returns true if we shall filter entries and false if no filtering is to be
    # done. hence, in this case we have to return the opposite of tag_match, because if we have a
    # tag match we want to return false to keep the entry
    return not tag_match


def __is_fsc_siz(entry, siz_control, hits_per_feed, filter_debug_object):
    """
    check if feed special code (fsc) contains a siz(e) filter
    and we have a condition that leads to filtering
    """
    from mlib.debug import add_to_debug_dict as debug_add_to_debug_dict
    from mlib.extract import feed_special_code as extract_feed_special_code

    filter_match = False

    # if size (siz) is set in `feed_special_code`
    if 'siz=' in entry['feed_special_code']:

        # get siz from special code if we already haven't done so
        if not entry['feed_id'] in siz_control:
            siz_control[entry['feed_id']] = int(
                extract_feed_special_code(entry['feed_special_code'], 'siz'))

        # make initial hits_per_feed entry
        if not entry['feed_id'] in hits_per_feed:
            hits_per_feed[entry['feed_id']] = 1
        # ... or increase the count by one if it already exsists
        else:
            hits_per_feed[entry['feed_id']] = hits_per_feed[entry['feed_id']] + 1

        # if we have reached the limit (check if hits is bigger than, instead of
        # 'bigger or equal' since we already have added the current entry)
        if hits_per_feed[entry['feed_id']] > siz_control[entry['feed_id']]:
            # build debug dict
            filter_debug_object = debug_add_to_debug_dict(
                {'entry_title': entry['entry_title'],
                 'entry_published_date_display': entry['entry_published_date_display'],
                 'feed_profile_id': entry['feed_profile_id'],
                 'siz_control['+entry['feed_id']+']': siz_control[entry['feed_id']],
                 'hits_per_feed['+entry['feed_id']+']': hits_per_feed[entry['feed_id']]},
                filter_debug_object, 'feed_special_code.siz', entry['entry_id'])
            filter_match = True

    return filter_match


def __is_filtered_by_list_size(db_data, entry, hits, filter_debug_object):
    """
    check if we have a list size filter set and we if have a condition that leads to filtering
    """
    from mlib.debug import add_to_debug_dict as debug_add_to_debug_dict

    filter_match = False

    # if a list size filter is set as a get parameter
    if db_data.parameters['list_size'] is not None:

        if hits >= int(db_data.parameters['list_size']):
            filter_debug_object = debug_add_to_debug_dict(
                {'entry_title': entry['entry_title'],
                 'entry_published_date_display': entry['entry_published_date_display'],
                 'feed_profile_id': entry['feed_profile_id'],
                 'count_limit_reached': db_data.parameters['list_size']},
                filter_debug_object, 'list_size', entry['entry_id'])
            filter_match = True

    return filter_match


def feed(
        db_data,
        input_feed
):
    """
    output content as json
    """
    from mlib.debug import add_to_debug_dict as debug_add_to_debug_dict

    processed_feed = {}
    processed_entries = []
    hits = 0
    feed_previous = ''
    entry_id_found = False
    entry_id_previous = ''
    entry_id_next = ''
    concurrent_entries = 1
    dupe_control = {}
    entries_per_main_category_control = {}
    entries_per_profile_control = {}
    filter_debug_object = {}
    siz_control = {}
    hits_per_feed = {}

    # loop through the array of entries stored in the result set
    for entry in input_feed['entries']:

        # check if the entry is banned
        if __is_banned(db_data, entry, filter_debug_object):
            continue

        # check if we have a feed specific filter mismatch
        if __is_fsf_mismatch(entry, filter_debug_object):
            continue

        # if an entry id filter is set
        if db_data.parameters['entry_id'] is not None:

            # if it is not the entry_id we are looking for...
            if db_data.parameters['entry_id'] != entry['entry_id']:

                filter_debug_object = debug_add_to_debug_dict(
                    {'entry_title': entry['entry_title'],
                     'entry_published_date_display': entry['entry_published_date_display'],
                     'feed_profile_id': entry['feed_profile_id'],
                     'input_entry_id': db_data.parameters['entry_id']},
                    filter_debug_object, 'entry_id', entry['entry_id'])

                # scenario one: it is an entry id prior to the one we are looking for
                if not entry_id_found:
                    entry_id_previous = entry['entry_id']

                    # since it is not the entry we are searching for we jump to the next one in the
                    # loop, and hence avoid keeping it
                    continue

                # scenario two: it is the entry id following the one we are looking for
                elif not entry_id_next:
                    entry_id_next = entry['entry_id']

            # if it is the entry_id we are searching for...
            else:
                entry_id_found = True

            # if we have a previous entry id stored we add it to this entry
            if entry_id_previous:
                entry['entry_id_previous'] = entry_id_previous

            # if this is 'the next entry' we update the previously found entry with info pointing
            # to this id
            if entry_id_next:
                processed_entries[0]['entry_id_next'] = entry_id_next
                continue

        # if a country code filter is set
        if __is_filtered_by_field('feed_country_code', db_data, entry, filter_debug_object):
            continue

        # if a main category filter is set
        if __is_filtered_by_field('feed_main_category_id', db_data, entry, filter_debug_object):
            continue

        # if a sub category filter is set
        if __is_filtered_by_field('feed_sub_category_id', db_data, entry, filter_debug_object):
            continue

        # if a profile filter is set
        if __is_filtered_by_field('feed_profile_id', db_data, entry, filter_debug_object):
            continue

        # if a feed source filter is set
        if __is_filtered_by_field('feed_source_id', db_data, entry, filter_debug_object):
            continue

        # if a feed id filter is set
        if __is_filtered_by_field('feed_id', db_data, entry, filter_debug_object):
            continue

        # if a 'max_concurrent_entries' (entries from same source, in a row) filter is set
        if db_data.parameters['max_concurrent_entries'] is not None:
            if feed_previous == entry['feed_id']:
                if concurrent_entries >= int(db_data.parameters['max_concurrent_entries']):
                    filter_debug_object = debug_add_to_debug_dict(
                        {'entry_title': entry['entry_title'],
                         'entry_published_date_display': entry['entry_published_date_display'],
                         'feed_profile_id': entry['feed_profile_id']},
                        filter_debug_object, 'max_concurrent_entries', entry['entry_id'])
                    continue
                else:
                    concurrent_entries = concurrent_entries + 1

        # if an exclude filter is set
        if __is_excluded(db_data, entry, filter_debug_object):
            continue

        # if a dupe filter is set
        if __is_duped(db_data, entry, dupe_control, filter_debug_object):
            continue

        # if a 'cl_feed_main_category_id' (limit to how many entries per main category)
        # filter is set
        if __is_limited_by_count(
                'feed_main_category_id', db_data, entry, entries_per_main_category_control,
                filter_debug_object):
            continue

        # if a 'cl_feed_profile_id' (limit to how many entries per profile) filter is set
        if __is_limited_by_count(
                'feed_profile_id', db_data, entry, entries_per_profile_control,
                filter_debug_object):
            continue

        # if we shall filter on tags
        if __is_filtered_by_tag(db_data, entry, filter_debug_object):
            continue

        # if size (siz) is set in `feed_special_code`
        if __is_fsc_siz(entry, siz_control, hits_per_feed, filter_debug_object):
            continue

        # if a list size filter is set as a get parameter
        if __is_filtered_by_list_size(db_data, entry, hits, filter_debug_object):
            continue

        processed_entries.append(entry)
        feed_previous = entry['feed_id']
        hits = hits + 1

    # add all existing feed elements to the return feed
    for i in input_feed:
        processed_feed[i] = input_feed[i]

    # add new and updated feed elements to the return feed
    processed_feed['entries'] = processed_entries
    if 'debug_object' in processed_feed and filter_debug_object:
        processed_feed['debug_object']['excluded_from_filtering'] = filter_debug_object
        processed_feed['debug_object']['hits_after_filter'] = hits

    return processed_feed


def test_feed():
    """
    test
    """
    # pylint: disable=too-few-public-methods
    from mlib.test_data import get_test_entries
    class DbData:
        """
        test
        """
        from mlib.parameters import get as parameters_get
        parameters = parameters_get()
        banned_entries = []
        banned_title_content = []
        banned_body_content = []
    db_data = DbData()
    feed_data = {'entries': get_test_entries()}
    # initial test
    try:
        processed_feed = feed(db_data, feed_data)
    except:
        import traceback
        raise ValueError(
            'filter.test_feed()',
            'initiation issue\n------------------\n' +
            traceback.format_exc() + '------------------')
    # a note on the test id: using the time of day. for example test id 2214 was written at 22:14
    # 2214 - test entry_id banning
    try:
        db_data.banned_entries = [1]
        processed_feed = feed(db_data, feed_data)
        if len(processed_feed['entries']) != 1:
            raise ValueError('filter.test_feed()', 'guru meditation 2214#1')
        if processed_feed['entries'][0]['entry_id'] != 2:
            raise ValueError('filter.test_feed()', 'guru meditation 2214#2')
        db_data.banned_entries = []
    except ValueError as error:
        raise ValueError(error.args[0], error.args[1])
    # 2222 - test title banning
    try:
        db_data.banned_title_content = ['title one']
        processed_feed = feed(db_data, feed_data)
        if processed_feed['entries'][0]['entry_id'] != 2:
            raise ValueError('filter.test_feed()', 'guru meditation 2222')
        db_data.banned_title_content = []
    except ValueError as error:
        raise ValueError(error.args[0], error.args[1])
    # 2237 - test body banning
    try:
        db_data.banned_body_content = ['body one', 'body twwo']
        processed_feed = feed(db_data, feed_data)
        if processed_feed['entries'][0]['entry_id'] != 2:
            raise ValueError('filter.test_feed()', 'guru meditation 2237')
        db_data.banned_body_content = []
    except ValueError as error:
        raise ValueError(error.args[0], error.args[1])
    # 2252 - test feed specific filter (fsf)
    try:
        # include body
        feed_data['entries'][0]['feed_special_code'] = 'fsf=ib.dopestoinfinity'
        processed_feed = feed(db_data, feed_data)
        if processed_feed['entries'][0]['entry_id'] != 2:
            raise ValueError('filter.test_feed()', 'guru meditation 2252#1')
        # exclude body
        feed_data['entries'][0]['feed_special_code'] = 'fsf=eb.one'
        processed_feed = feed(db_data, feed_data)
        if processed_feed['entries'][0]['entry_id'] != 2:
            raise ValueError('filter.test_feed()', 'guru meditation 2252#2')
        # include title
        feed_data['entries'][0]['feed_special_code'] = 'fsf=it.monstermagnet'
        processed_feed = feed(db_data, feed_data)
        if processed_feed['entries'][0]['entry_id'] != 2:
            raise ValueError('filter.test_feed()', 'guru meditation 2252#3')
        # exclude title
        feed_data['entries'][0]['feed_special_code'] = 'fsf=et.title one'
        processed_feed = feed(db_data, feed_data)
        if processed_feed['entries'][0]['entry_id'] != 2:
            raise ValueError('filter.test_feed()', 'guru meditation 2252#4')
        feed_data['entries'][0]['feed_special_code'] = ''
    except ValueError as error:
        raise ValueError(error.args[0], error.args[1])
    # 2210 - test entry_id filter
    try:
        db_data.parameters['entry_id'] = 2
        processed_feed = feed(db_data, feed_data)
        if len(processed_feed['entries']) != 1:
            raise ValueError('filter.test_feed()', 'guru meditation 2210')
        db_data.parameters['entry_id'] = None
    except ValueError as error:
        raise ValueError(error.args[0], error.args[1])
    # 2311 - test country code filter
    try:
        db_data.parameters['feed_country_code'] = 'nor'
        processed_feed = feed(db_data, feed_data)
        if processed_feed['entries'][0]['entry_id'] != 2:
            raise ValueError('filter.test_feed()', 'guru meditation 2311')
        db_data.parameters['feed_country_code'] = None
    except ValueError as error:
        raise ValueError(error.args[0], error.args[1])
    # main cat filter test not implemented
    # sub cat filter test not implemented
    # profile filter test not implemented
    # feed source filter test not implemented
    # feed id filter test not implemented
    # 1325 - test max_concurrent_entries
    try:
        db_data.parameters['max_concurrent_entries'] = 1
        processed_feed = feed(db_data, feed_data)
        if len(processed_feed['entries']) != 1:
            raise ValueError('filter.test_feed()', 'guru meditation 1325')
        db_data.parameters['max_concurrent_entries'] = None
    except ValueError as error:
        raise ValueError(error.args[0], error.args[1])
    # 1334 - test exclude filter
    try:
        db_data.parameters['exclude'] = 'feed_country_code.jap'
        processed_feed = feed(db_data, feed_data)
        if processed_feed['entries'][0]['entry_id'] != 2:
            raise ValueError('filter.test_feed()', 'guru meditation 1334#1')
        db_data.parameters['exclude'] = 'feed_id.fone'
        processed_feed = feed(db_data, feed_data)
        if processed_feed['entries']:
            raise ValueError('filter.test_feed()', 'guru meditation 1334#2')
        db_data.parameters['exclude'] = 'feed_main_category_id.maincat1'
        processed_feed = feed(db_data, feed_data)
        if processed_feed['entries'][0]['entry_id'] != 2:
            raise ValueError('filter.test_feed()', 'guru meditation 1334#3')
        db_data.parameters['exclude'] = 'feed_sub_category_id.subcat1'
        processed_feed = feed(db_data, feed_data)
        if processed_feed['entries'][0]['entry_id'] != 2:
            raise ValueError('filter.test_feed()', 'guru meditation 1334#4')
        db_data.parameters['exclude'] = 'feed_profile_id.180876'
        processed_feed = feed(db_data, feed_data)
        if processed_feed['entries']:
            raise ValueError('filter.test_feed()', 'guru meditation 1334#5')
        db_data.parameters['exclude'] = 'feed_source_id.fsione'
        processed_feed = feed(db_data, feed_data)
        if processed_feed['entries'][0]['entry_id'] != 2:
            raise ValueError('filter.test_feed()', 'guru meditation 1334#6')
        db_data.parameters['exclude'] = None
    except ValueError as error:
        raise ValueError(error.args[0], error.args[1])
    # dupe filter title check filter test not implemented
    # 1346 - test dupe filter link check
    try:
        db_data.parameters['avoid_dupes'] = 'entry_link'
        processed_feed = feed(db_data, feed_data)
        if len(processed_feed['entries']) != 1:
            raise ValueError('filter.test_feed()', 'guru meditation 1346')
        db_data.parameters['avoid_dupes'] = None
    except ValueError as error:
        raise ValueError(error.args[0], error.args[1])
    # entries per main category filter test not implemented
    # 1348 - test entries per profile
    try:
        db_data.parameters['cl_feed_profile_id'] = '1'
        processed_feed = feed(db_data, feed_data)
        if len(processed_feed['entries']) != 1:
            raise ValueError('filter.test_feed()', 'guru meditation 1348')
        db_data.parameters['cl_feed_profile_id'] = None
    except ValueError as error:
        raise ValueError(error.args[0], error.args[1])
    # 1352 - test filter on tags
    try:
        db_data.parameters['entry_tags'] = 'twotone'
        processed_feed = feed(db_data, feed_data)
        if processed_feed['entries'][0]['entry_id'] != 2:
            raise ValueError('filter.test_feed()', 'guru meditation 1352')
        db_data.parameters['entry_tags'] = None
    except ValueError as error:
        raise ValueError(error.args[0], error.args[1])
    # 1353 - test filter on siz
    try:
        feed_data['entries'][0]['feed_special_code'] = 'siz=1'
        feed_data['entries'][1]['feed_special_code'] = 'siz=1'
        processed_feed = feed(db_data, feed_data)
        if len(processed_feed['entries']) != 1:
            raise ValueError('filter.test_feed()', 'guru meditation 1353')
        feed_data['entries'][0]['feed_special_code'] = ''
        feed_data['entries'][1]['feed_special_code'] = ''
    except ValueError as error:
        raise ValueError(error.args[0], error.args[1])
    # 1359 - test filter on list_size
    try:
        db_data.parameters['list_size'] = '1'
        processed_feed = feed(db_data, feed_data)
        if len(processed_feed['entries']) != 1:
            raise ValueError('filter.test_feed()', 'guru meditation 1359')
        db_data.parameters['list_size'] = None
    except ValueError as error:
        raise ValueError(error.args[0], error.args[1])
