# muninn

[![build status](https://gitlab.com/klevstul/muninn/badges/master/build.svg)](https://gitlab.com/klevstul/muninn/pipelines)

<img src="https://gitlab.com/klevstul/muninn/raw/master/additional_resources/graphics/muninn.png" width="500">



## what is hom?

*hom* is an abbreviation for **huginn ok muninn**. it's a solution for managing content where **huginn** (thought) being the font-end and **muninn** (mind) the back-end. instead of storing content in a database it's simply presenting content stored elswhere. hence, it's **n**ot **o**ne **t**raditional **c**ontent **m**anagement **s**olution. it's a **notCMS**. the front-end **huginn** is built lightweight, in pure vanilla typescript, independent from any js library. the back-end **muninn** is a feature rich api (application programming interface) / rpc (remote procedure call) solution built in python.

to view **hom** in action visit [hom.is](https://hom.is)  .



## technical documentation

all information about **hom** is gathered in the [muninn wiki](https://gitlab.com/klevstul/muninn/wikis/).



## related repositories

see all [hom related repositories](https://gitlab.com/klevstul/muninn/wikis/repository-overview).



## frequently asked questions

[list-to-be-created]
