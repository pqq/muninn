
import sys
import pickle

pickle_content = {'hello': 'world!'}
fname = 'out.pickle'

with open(fname, 'wb') as handle:
    pickle.dump(pickle_content, handle)
handle.close()
