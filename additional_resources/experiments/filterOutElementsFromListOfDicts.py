# http://stackoverflow.com/questions/42318698/how-to-filter-out-elements-from-a-list-of-dictionaries-with-repeating-keys-and-d

import timeit


def methodOne():
    # copying / appending to a new array
    listOfDic = [
          {'firstElement': 'a', 'secondElement': '1', 'thirdElement': 'i'}
        , {'firstElement': 'b', 'secondElement': '2', 'thirdElement': 'ii'}
        , {'firstElement': 'c', 'secondElement': '3', 'thirdElement': 'iii'}
    ]
    filteredListOfDic = []
    for row in listOfDic:
        filteredListOfDic.append({
              'firstElement': row['firstElement']
            , 'thirdElement': row['thirdElement']
        })

def methodTwoA():
    # list comprehension
    listOfDic = [
          {'firstElement': 'a', 'secondElement': '1', 'thirdElement': 'i'}
        , {'firstElement': 'b', 'secondElement': '2', 'thirdElement': 'ii'}
        , {'firstElement': 'c', 'secondElement': '3', 'thirdElement': 'iii'}
    ]
    filteredListOfDic = []
    filteredListOfDic = [{"firstElement": row["firstElement"], "thirdElement": row['thirdElement']} for row in listOfDic]
    return filteredListOfDic

def methodTwoB():
    # list comprehension
    listOfDic = [
          {'firstElement': 'a', 'secondElement': '1', 'thirdElement': 'i'}
        , {'firstElement': 'b', 'secondElement': '2', 'thirdElement': 'ii'}
        , {'firstElement': 'c', 'secondElement': '3', 'thirdElement': 'iii'}
    ]
    filteredListOfDic = []
    filteredListOfDic = [{k: d[k] for k in ('firstElement', 'thirdElement')} for d in listOfDic]
    return filteredListOfDic

def methodThree():
    # deleting elements from existing list
    listOfDic = [
          {'firstElement': 'a', 'secondElement': '1', 'thirdElement': 'i'}
        , {'firstElement': 'b', 'secondElement': '2', 'thirdElement': 'ii'}
        , {'firstElement': 'c', 'secondElement': '3', 'thirdElement': 'iii'}
    ]

    for dictionary in listOfDic:
        del dictionary["secondElement"]
    return listOfDic

print '1.000.000 loops:'
print 'original method     : ' + str(timeit.timeit(methodOne, number=1000000)) + 's'
print 'list comprehension a: ' + str(timeit.timeit(methodTwoA, number=1000000)) + 's'
print 'list comprehension b: ' + str(timeit.timeit(methodTwoB, number=1000000)) + 's'
print 'deletion            : ' + str(timeit.timeit(methodThree, number=1000000)) + 's'
