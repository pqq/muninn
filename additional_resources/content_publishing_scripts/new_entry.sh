#!/bin/bash

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# what:        creates a new entry (of type 'news' or 'orec')
# author:      frode burdal klevstul (frode at klevstul dot com)
# started:     apr 2019
#
# requires:    n/a
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

# define variables
news_dir=/media/dataDrive/Dropbox/gitlab/thisworld.is/content/news
orec_dir=/media/dataDrive/Dropbox/gitlab/thisworld.is/content/orec
pics_dir=/media/dataDrive/twi/new_entry_pictures
pics_in=/media/dataDrive/twi/pics_in
pics_out=/media/dataDrive/twi/pics_out
new_file_name=readme.md
album_file_name=album.md
script_dir=/media/dataDrive/Dropbox/gitlab/muninn/additional_resources/content_publishing_scripts
base_dir=''

# check the script is run with correct arguments
this_file_name=`basename "$0"`
if [ $# -ne 4 ]; then
    echo "usage: $this_file_name {news,orec} [YYDDMM] 'title goes here' 'and here is the body'"
    exit 1
fi

# get arguments from cli
type=$1
date=$2
title_orig=$3
title=${title_orig// /_}
body=$4

case $type in
   "news") base_dir=$news_dir;;
   "orec") base_dir=$orec_dir;;
   *)
        echo "error: '$type' is an unknown value. it must be either 'news' or 'orec'"
        exit
        ;;
esac

# set correct folders
new_folder=$date-$title!
new_dir=$base_dir/$new_folder

# make sure the new directory that we shall create doesn't exist from before
if [ -d "$new_dir" ]; then
    echo "error: directory exsist '$new_dir'"
    exit
fi

# create new entry
mkdir $new_dir
content="### $title_orig"
echo $content > $new_dir/$new_file_name
echo "" >> $new_dir/$new_file_name
echo "$body  " >> $new_dir/$new_file_name
echo "" >> $new_dir/$new_file_name

# include pictures, if any
if [ "$(ls -A $pics_dir)" ]; then

    # check that picture dirs for preparing pictures is empty
    if [ "$(ls -A $pics_in)" ]; then
        echo "error: '$pics_in' is not empty."
        exit
    fi
    if [ "$(ls -A $pics_out)" ]; then
        echo "error: '$pics_out' is not empty."
        exit
    fi

    echo "<center>" >> $new_dir/$new_file_name

    mv $pics_dir/* $pics_in
    $script_dir/prepare_pictures.sh run
    mkdir $new_dir/assets
    mv $pics_out/* $new_dir/assets/
    $script_dir/create_picture_album.sh $type $new_folder
    cat $new_dir/$new_file_name $new_dir/$album_file_name > $new_dir/merged.md
    mv $new_dir/merged.md $new_dir/$new_file_name

    echo "</center>" >> $new_dir/$new_file_name

    trash-put $new_dir/$album_file_name
    trash-put $pics_in/*
fi
