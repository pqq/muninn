#!/bin/bash

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# what:        creates an album file given there are pictures in a given orec dir
# author:      frode burdal klevstul (frode at klevstul dot com)
# started:     apr 2019
#
# requires:    n/a
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

outfile=album.md
news_dir=/media/dataDrive/Dropbox/gitlab/thisworld.is/content/news
orec_dir=/media/dataDrive/Dropbox/gitlab/thisworld.is/content/orec
base_dir=''

this_file_name=`basename "$0"`
if [ $# -ne 2 ]; then
    echo "usage: '$this_file_name {news,orec} [directory]'"
    exit 1
fi

type=$1
directory=$2

case $type in
   "news") base_dir=$news_dir;;
   "orec") base_dir=$orec_dir;;
   *)
        echo "error: '$type' is an unknown value. it must be either 'news' or 'orec'"
        exit
        ;;
esac

this_dir=$base_dir/$2

assets_dir=$this_dir/assets
outfile_full=$this_dir/$outfile

if [ -f $outfile_full ]; then
    echo "error: '$outfile_full' already exists"
    exit
fi

if [ ! -d "$assets_dir" ]; then
    echo "error: unknown assets directory '$assets_dir'"
    exit
fi

for file in $assets_dir/*; do
    if [ -f "$file" ]; then
        filename=$(basename -- "$file")
        echo "![](assets/$filename)  " >> $outfile_full
    fi
done
