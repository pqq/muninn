#!/bin/bash

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# what:        reduces picture size and otherwise prepare them for publishing
# author:      frode burdal klevstul (frode at klevstul dot com)
# started:     apr 2019
#
# requires:    trash-cli
#              ffmpeg
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

base_dir=/media/dataDrive/twi
ffmpeg_maxwidth=1800
logo=/media/dataDrive/Dropbox/gitlab/thisworld.is/appendix/graphics/logo_100.png
source_dir=$base_dir/pics_in
target_dir=$base_dir/pics_out
this_file_name=`basename "$0"`

if [ $# -ne 1 ]; then
    echo "usage: $this_file_name {setup,run}"
    exit 1
fi
operation=$1

# src: https://stackoverflow.com/questions/21919182/bash-not-in-case
shopt -s extglob
case $operation in
    !(setup|run))
        echo "error: '$operation' must be either 'setup' or 'run'"
        exit
        ;;
esac
shopt -u extglob


if [ ! -e "$logo" ]; then
    echo "error: missing logo file '$logo'"
    exit
fi

if [ ! -d "$base_dir" ]; then
    echo "error: unknown base directory '$base_dir'"
    exit
fi

# setup directories / make sure directories exists
case $operation in
    setup)
        if [ -d "$source_dir" ]; then
            echo "error: directory '$source_dir' already exists"
            exit
        fi
        if [ -d "$target_dir" ]; then
            echo "error: directory '$target_dir' already exists"
            exit
        fi
        mkdir $source_dir
        mkdir $target_dir
        ;;
    run)
        if [ ! -d "$source_dir" ]; then
            echo "error: missing directory '$source_dir'"
            echo "run '$this_file_name setup' to set up initial directories"
            exit
        fi
        if [ ! -d "$target_dir" ]; then
            echo "error: missing directory '$source_dir'"
            echo "create this directory and run this script once again"
            exit
        fi
        # check that source dir is not empty
        if [ ! "$(ls -A $source_dir)" ]; then
            echo "error: '$source_dir' is empty."
            exit
        fi
        # check that target dir is empty
        if [ "$(ls -A $target_dir)" ]; then
            echo "error: '$target_dir' is not empty."
            exit
        fi
        ;;
esac

# run operation
shopt -s extglob
if [ $operation == "run" ]; then

    # https://stackoverflow.com/questions/216995/how-can-i-use-inverse-or-negative-wildcards-when-pattern-matching-in-a-unix-linu?answertab=votes#tab-top
    cp $source_dir/*.@(jpg|jpeg|png) $target_dir/

    for file in $target_dir/*; do
        if [ -f "$file" ]; then
            # https://stackoverflow.com/questions/965053/extract-filename-and-extension-in-bash
            filename=$(basename -- "$file")
            extension="${filename##*.}"
            filename="${filename%.*}"
            append="_."
            outfile=$target_dir/$filename$append$extension

            # orientation is lost when image is manipulated. hence, we need to set the orientation
            # for the manipulated file to the same as the original
            #   https://gitlab.com/klevstul/muninn/issues/122
            orientation=$(exiftool -Orientation -n -S $file | grep -Eo '[0-9]{1,4}')

            # scale image
            #   https://trac.ffmpeg.org/wiki/Scaling
            ffmpeg -i $file -vf "scale='min($ffmpeg_maxwidth,iw)':-1" $outfile

            # appending logo
            ffmpeg -y -i $outfile -i $logo -filter_complex 'overlay=main_w-120:main_h-overlay_h-10' $outfile

            # set orientation value for the new image
            exiftool -n -Orientation=$orientation $outfile

            # deleting old files
            trash-put $file
            trash-put $outfile\_original
        fi
    done

fi
shopt -u extglob
