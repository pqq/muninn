#!/bin/bash

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# what:        uninstall a given version of hom
# author:      frode burdal klevstul (frode at klevstul dot com)
# started:     mar 2019
#
# requires:    trash-cli (https://www.archlinux.org/packages/community/any/trash-cli/)
#              git
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

if [ "$EUID" -eq 0 ]
  then echo "error: run as 'blazar'"
  exit
fi

this_file_name=`basename "$0"`

if [ $# -ne 1 ]; then
    echo usage: $this_file_name [version]
    exit 1
fi

version=$1

dir_to_delete=/var/www/hom/prod/$version

# make sure that the base directory exist
if [ ! -d "$dir_to_delete" ]; then
    echo error: directory $dir_to_delete does not exist
    exit 1
fi

trash-put $dir_to_delete
