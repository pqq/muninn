#!/bin/bash

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# what:        set up the hom enviroment for the first time
# author:      frode burdal klevstul (frode at klevstul dot com)
# started:     mar 2019
#
# requires:    python 3.7
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

if [ "$EUID" -eq 0 ]
  then echo "error: run as 'blazar'"
  exit
fi

target_dir=/var/www/hom

if [ -d "$target_dir/prod" ]; then
    echo error: $target_dir/prod already exist
    exit 1
fi

python3.7 -m venv $target_dir
mkdir -p $target_dir/prod
source /var/www/hom/bin/activate
pip install feedparser markdown
