#!/bin/bash

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# what:        downloads and install latest prod build of hom on prod server
#              using https://gitlab.com/klevstul/midgard/ as source
# author:      frode burdal klevstul (frode at klevstul dot com)
# started:     mar 2019
#
# requires:    trash-cli (https://www.archlinux.org/packages/community/any/trash-cli/)
#              git
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

if [ "$EUID" -eq 0 ]
  then echo "error: run as 'blazar'"
  exit
fi

this_file_name=`basename "$0"`

if [ $# -ne 1 ]; then
    echo usage: $this_file_name [version]
    echo example: \'$this_file_name v1v1\'
    exit 1
fi

version=$1

target_dir=/var/www/hom/prod/

# make sure that the base directory exist
if [ ! -d "$target_dir" ]; then
    echo error: the base directory $target_dir does not exist
    exit 1
fi

target_dir=$target_dir$version

# make sure the new version directory doesn't exist
if [ -d "$target_dir" ]; then
  echo error: the directory $target_dir already exists
  exit 1
fi

# create new directory, download, prepare and copy latest prod build
if [ ! -d "$target_dir" ]; then
    echo downloading and installing hom into the new directory $target_dir
    mkdir -p $target_dir
    cd /tmp/
    trash-put /tmp/midgard
    git clone https://gitlab.com/klevstul/midgard.git
    cp -r /tmp/midgard/environments/prod/* /var/www/hom/prod/$version/
    chmod 755 /var/www/hom/prod/$version/api/muninn.py
    chmod o+w /var/www/hom/prod/$version/log/*.log
fi
