#!/bin/bash

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# what:        releases a given prod version of hom
# author:      frode burdal klevstul (frode at klevstul dot com)
# started:     mar 2019
#
# requires:    trash-cli (https://www.archlinux.org/packages/community/any/trash-cli/)
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

if [ "$EUID" -eq 0 ]
  then echo "error: run as 'blazar'"
  exit
fi

this_file_name=`basename "$0"`

if [ $# -ne 2 ]; then
    echo usage: $this_file_name \{latest,new\} [version]
    echo example: \'$this_file_name latest v1v1\'
    exit 1
fi

latest_new=$1
version=$2

case $latest_new in
    latest|new)
        echo setting version $version to be the $latest_new version ;;
   *)
        echo error: \'$latest_new\' must be \'latest\' or \'new\' ;;
esac

base_dir=/var/www/hom/prod
source_dir=$base_dir/$version/

# make sure that the base directory exist
if [ ! -d "$source_dir" ]; then
    echo error: source directory $source_dir does not exist
    exit 1
fi

trash-put $base_dir/$latest_new

ln -s $source_dir $base_dir/$latest_new
