#!/bin/bash

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# what:        downloads and "installs" hotfixed version of feedparser
# author:      frode burdal klevstul (frode at klevstul dot com)
# started:     mar 2019
#
# requires:    trash-cli (https://www.archlinux.org/packages/community/any/trash-cli/)
#              wget
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

if [ "$EUID" -eq 0 ]
  then echo "error: run as 'blazar'"
  exit
fi

trash-put /tmp/feedparser.py
cd /tmp/
wget https://gitlab.com/klevstul/muninn/raw/master/additional_resources/feedparser_hotfix/feedparser.py
cp /tmp/feedparser.py /var/www/hom/lib/python3.7/site-packages/.
