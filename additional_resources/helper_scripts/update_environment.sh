#!/bin/bash

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# what:        update hom environment
# author:      frode burdal klevstul (frode at klevstul dot com)
# started:     mar 2019
#
# requires:    trash-cli (https://www.archlinux.org/packages/community/any/trash-cli/)
#              git
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

if [ "$EUID" -eq 0 ]
  then echo "error: run as 'blazar'"
  exit
fi

source /var/www/hom/bin/activate
pip install --upgrade pip feedparser markdown
