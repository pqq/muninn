#!/bin/bash

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# what:        tests a given muninn version
# author:      frode burdal klevstul (frode at klevstul dot com)
# started:     mar 2019
#
# requires:
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

if [ "$EUID" -eq 0 ]
  then echo "error: run as 'blazar'"
  exit
fi

this_file_name=`basename "$0"`
target_dir=/var/www/hom

if [ $# -ne 1 ]; then
    echo usage: $this_file_name [version]
    exit 1
fi

version=$1

python3.7 -m venv $target_dir
source /var/www/hom/bin/activate
python /var/www/hom/prod/$version/api/testdrive.py
deactivate
