#!/bin/bash

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# what:        downloads all needed scripts for hom
# author:      frode burdal klevstul (frode at klevstul dot com)
# started:     mar 2019
#
# requires:    n/a
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

if [ "$EUID" -ne 0 ]
  then echo "error: run as 'root'"
  exit
fi

trash-put /tmp/*.sh
cd /tmp/
wget https://gitlab.com/klevstul/muninn/raw/master/additional_resources/prod_server_setup_scripts/00_download_all_scripts.sh
wget https://gitlab.com/klevstul/muninn/raw/master/additional_resources/prod_server_setup_scripts/10_run_me_first.sh
wget https://gitlab.com/klevstul/muninn/raw/master/additional_resources/prod_server_setup_scripts/20_https_part_1.sh
wget https://gitlab.com/klevstul/muninn/raw/master/additional_resources/prod_server_setup_scripts/21_https_part_2.sh
wget https://gitlab.com/klevstul/muninn/raw/master/additional_resources/prod_server_setup_scripts/90_update_nginx_conf.sh
wget https://gitlab.com/klevstul/muninn/raw/master/additional_resources/prod_server_setup_scripts/91_upd_nginx_conf_and_restart.sh
chmod 755 *.sh
trash-put /tmp/muninn
mkdir /tmp/muninn
cd /tmp/muninn
wget https://gitlab.com/klevstul/muninn/raw/master/additional_resources/helper_scripts/feedparser_hotfix.sh
wget https://gitlab.com/klevstul/muninn/raw/master/additional_resources/helper_scripts/install_latest_prod_build.sh
wget https://gitlab.com/klevstul/muninn/raw/master/additional_resources/helper_scripts/new_environment_setup.sh
wget https://gitlab.com/klevstul/muninn/raw/master/additional_resources/helper_scripts/release_version.sh
wget https://gitlab.com/klevstul/muninn/raw/master/additional_resources/helper_scripts/test_version.sh
wget https://gitlab.com/klevstul/muninn/raw/master/additional_resources/helper_scripts/uninstall_version.sh
wget https://gitlab.com/klevstul/muninn/raw/master/additional_resources/helper_scripts/update_environment.sh
chmod 755 *.sh
