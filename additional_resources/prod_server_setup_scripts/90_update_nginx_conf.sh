#!/bin/bash

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# what:        update nginx scrips
# author:      frode burdal klevstul (frode at klevstul dot com)
# started:     mar 2019
#
# requires:    n/a
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

if [ "$EUID" -ne 0 ]
  then echo "error: run as 'root'"
  exit
fi

cd /tmp/
trash-put hom.is.conf
wget https://gitlab.com/klevstul/muninn/raw/master/additional_resources/prod_server_files/nginx/hom.is.conf
cp /tmp/hom.is.conf /etc/nginx/sites-available/hom.is.conf
ln -s /etc/nginx/sites-available/hom.is.conf /etc/nginx/sites-enabled/
chown blazar:blazar /etc/nginx/sites-available/hom.is.conf
