#!/bin/bash

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# author:      frode burdal klevstul (frode at klevstul dot com)
# started:     mar 2019
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

if [ "$EUID" -ne 0 ]
  then echo "error: run as 'root'"
  exit
fi

# ---
# let's encrypt ssl/https
# ---
apt -y install git-core bc
cd /tmp/
git clone https://github.com/Neilpang/acme.sh.git
cd acme.sh/
./acme.sh --install

# ---
# manual steps
# ---
echo "------------------------------------------------------------------"
echo "MANUAL INSTRUCTIONS TO FOLLOW:"
echo "------------------------------------------------------------------"
echo "visit https://cloud.digitalocean.com/account/api/tokens?i=d0165a"
echo "  - generate new token"
echo "    - token name: 'acme.sh' scope: 'r+w'"
echo "    - save token to clipboard as you'll need it for part 2 of"
echo "      setting up https"
echo "    - (save token to lastpass")
echo "- log out of the server and back in again (for acme.sh to be activated)"
echo "------------------------------------------------------------------"
