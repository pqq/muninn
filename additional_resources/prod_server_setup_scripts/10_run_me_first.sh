#!/bin/bash

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# what:        sets up a new production server for hom (huginnokmuninn)
# author:      frode burdal klevstul (frode at klevstul dot com)
# started:     mar 2019
#
# requires:    n/a
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

if [ "$EUID" -ne 0 ]
  then echo "error: run as 'root'"
  exit
fi

# ---
# update server
# ---
apt update
apt -y full-upgrade


# ---
# create a new non-root user
# blazar: https://en.wikipedia.org/wiki/Blazar or https://en.wikipedia.org/wiki/Bob_Lazar
# ---
adduser --disabled-password --gecos "" blazar
mkdir /home/blazar/.ssh
cp /root/.ssh/authorized_keys /home/blazar/.ssh/
chown blazar:blazar /home/blazar/.ssh/
chown blazar:blazar /home/blazar/.ssh/authorized_keys


# ---
# misc installations
#   trash-cli                                   - used in misc scripts
#   nginx                                       - webserver
#   libffi-dev zlib1g-dev libssl-dev gcc make   - for python 3.7
#   fcgiwrap                                    - fast cgi
# ---
apt -y install trash-cli nginx libffi-dev zlib1g-dev libssl-dev gcc make fcgiwrap


# ---
# nginx
# ---
systemctl enable nginx # enable on startup

# change mode so given dirs are writable by blazar (which is nifty)
chmod o+w /etc/nginx/sites-enabled/
chmod o+w /etc/nginx/sites-available/
chmod o+w /etc/nginx/nginx.conf
chmod o+w /var/www/

# download and set up site configurations
cd /tmp/
./90_update_nginx_conf.sh

# set up initial directories and files
mkdir /var/www/hom
chown blazar:blazar /var/www/hom

# install favicon
wget https://gitlab.com/klevstul/muninn/raw/master/additional_resources/graphics/frodr.ico
mv frodr.ico /var/www/favicon.ico

# ---
# python 3.7
# ---
cd /tmp/    # we should already be inside this dir, however, just in case...
wget https://www.python.org/ftp/python/3.7.3/Python-3.7.3.tgz
tar xzf Python-3.7.3.tgz
cd Python-3.7.3/
./configure --enable-optimizations
make altinstall
