#!/bin/bash

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# what:        update nginx scrips and restart the web server
# author:      frode burdal klevstul (frode at klevstul dot com)
# started:     mar 2019
#
# requires:    n/a
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

if [ "$EUID" -ne 0 ]
  then echo "error: run as 'root'"
  exit
fi

/tmp/90_update_nginx_conf.sh
nginx -t && systemctl restart nginx
