#!/bin/bash

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# author:      frode burdal klevstul (frode at klevstul dot com)
# started:     mar 2019
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

if [ "$EUID" -ne 0 ]
  then echo "error: run as 'root'"
  exit
fi

# ---
# http setup part two
# ---

this_file_name=`basename "$0"`

if [ $# -ne 1 ]; then
    echo usage: $this_file_name [TOKEN]
    exit 1
fi

token=$1

export DO_API_KEY="$token"
/root/.acme.sh/acme.sh --issue --dns dns_dgon -d hom.is -d *.hom.is -k ec-384
mkdir -p /etc/nginx/acme.sh/hom.is
/root/.acme.sh/acme.sh --install-cert -d hom.is --ecc --cert-file /etc/nginx/acme.sh/hom.is/cert.pem --key-file /etc/nginx/acme.sh/hom.is/key.pem --fullchain-file /etc/nginx/acme.sh/hom.is/fullchain.pem --reloadcmd "systemctl reload nginx.service"

# ---
# manual steps
# ---
echo "------------------------------------------------------------------"
echo "MANUAL INSTRUCTIONS TO FOLLOW:"
echo "------------------------------------------------------------------"
echo "some manual steps needs to be made by you:"
echo "- run 'export TERM=xterm'"
echo "- run 'nano /etc/nginx/nginx.conf'"
echo "   locate and uncomment the line 'server_names_hash_bucket_size 64;'"
echo "- run 'nginx -t' (to testing the config)"
echo "- run 'systemctl restart nginx' or 'systemctl reload nginx'"
echo "------------------------------------------------------------------"
