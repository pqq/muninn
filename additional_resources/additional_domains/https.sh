#!/bin/bash

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# author:      frode burdal klevstul (frode at klevstul dot com)
# started:     apr 2019
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

domain="thisworld.is"

if [ "$EUID" -ne 0 ]
  then echo "error: run as 'root'"
  exit
fi

this_file_name=`basename "$0"`

if [ $# -ne 1 ]; then
    echo usage: $this_file_name [TOKEN]
    exit 1
fi

token=$1

read -r -p "create certs for $domain - are you sure? [y/N] " response
if [[ ! "$response" =~ ^([yY][eE][sS]|[yY])+$ ]]
then
    exit 1
fi

export DO_API_KEY="$token"
/root/.acme.sh/acme.sh --issue --dns dns_dgon -d $domain -d *.$domain -k ec-384
mkdir -p /etc/nginx/acme.sh/$domain
/root/.acme.sh/acme.sh --install-cert -d $domain --ecc \
--cert-file /etc/nginx/acme.sh/$domain/cert.pem --key-file /etc/nginx/acme.sh/$domain/key.pem \
--fullchain-file /etc/nginx/acme.sh/$domain/fullchain.pem \
--reloadcmd "systemctl reload nginx.service"
