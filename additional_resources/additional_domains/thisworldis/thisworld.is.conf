# - - - - - - - - - - - - - - - - - - -
# configuration of thisworld.is
# by frode burdal klevstul
# - - - - - - - - - - - - - - - - - - -

# enforce ssl and bare domain (without www) for http requests,
# as $server_name will return the first value given
server {
    listen 80;
    server_name thisworld.is www.thisworld.is;
    return 301 https://$server_name$request_uri;
}

# www to bare domain for https requests
server {
    listen 443 ssl http2;
    listen [::]:443 ssl http2;
    ssl on;
    ssl_certificate /etc/nginx/acme.sh/thisworld.is/fullchain.pem;
    ssl_certificate_key /etc/nginx/acme.sh/thisworld.is/key.pem;
    ssl_trusted_certificate /etc/nginx/acme.sh/thisworld.is/cert.pem;
    server_name www.thisworld.is;
    return 301 $scheme://thisworld.is$request_uri;
}

server {
    listen 443 ssl http2;
    listen [::]:443 ssl http2;

    ssl on;
    ssl_certificate /etc/nginx/acme.sh/thisworld.is/fullchain.pem;
    ssl_certificate_key /etc/nginx/acme.sh/thisworld.is/key.pem;
    ssl_trusted_certificate /etc/nginx/acme.sh/thisworld.is/cert.pem;

	root /var/www/hom/prod/latest;
    index index.html;
    server_name thisworld.is;

    # append mandatory service_id for hom
    location = / {
        return 301 https://thisworld.is/app/?/service_id=twi;
    }

	# prevent the /log and /database directories from being accessible online
    location ~* /(log|database)/ {
        deny all;
   		return 404;
    }

    # fast cgi
	location ~* \.(pl|py)$ {
     	gzip off;
     	fastcgi_pass  unix:/var/run/fcgiwrap.socket;
     	include /etc/nginx/fastcgi_params;
     	fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
    }

    # for testing of newly installed hom versions
	location /new {
		alias /var/www/hom/prod/new;
	}

}
